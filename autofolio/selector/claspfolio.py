'''
Created on Dec 9, 2014

@author: Marius Lindauer
'''

import shutil
import os
import logging
import csv

from subprocess import Popen

class ClaspfolioSpecifics(object):
    '''
        adjusts pcs and so on specifically for claspfolio
    '''

    def __init__(self):
        '''
        Constructor
        '''
        
        self.DEFAULT = "-@1:F:aspeed-opt 'yes' -@1:F:concentrate 'no' -@1:approach 'CLASSVOTER' -@1:approx-weights 'None' -@1:aspeed:time-limit '300' -@1:classifier 'RANDOMFOREST' -@1:contr-filter '0.0' -@1:impute 'none' -@1:max-feature-time '60' -@1:max-solver '3' -@1:mem-limit '2000' -@1:normalize 'zscore' -@1:opt-mode '1' -@1:performance_trans 'None' -@1:separator 'W' -@1:solver 'selectors/claspfolio-2/src/trainer/main_train.py' -@1:time-pre-solvers '256' -@1:voting:rf-criterion 'entropy' -@1:voting:rf-max_features 'sqrt' -@1:voting:rf-min_samples_leaf '10'"
        
    def extend_pcs(self, pcs_file, scenario, metainfo, new_pcs, add_features=False, add_groups=False, add_algorithms=False, mem_limit=2000):
        '''
            copies existing pcs file and extend it by
             a) pcs by scenario
             b) features as binary variables (only if add_features==True)
             c) feature groups as binary variables (only if add_groups==True)
             d) algorithms as binary variables (only if add_algorithms==True)
            ARGS:
                pcs: pcs file
                scenario: directory of ASlib scenario
                metainfo: info from description.txt in ASlib scenario
        '''
        
        shutil.copy(pcs_file, new_pcs)
        
        with open(new_pcs, "a") as fp:
            fp.write("\n")
            fp.write("@1:aslib-dir{%s}[%s]\n" %(scenario, scenario))
            
            if add_features:
                features = set()
                features = features.union(metainfo.features_deterministic)
                features = features.union(metainfo.features_stochastic)
                feature_group_dict = metainfo.feature_group_dict
                default_steps = metainfo.feature_steps
                unused_features = set()
                unused_steps = set(feature_group_dict.keys()).difference(set(default_steps))
                for u_step in unused_steps:
                    not_processed_features = feature_group_dict[u_step]
                    unused_features = unused_features.union(set(not_processed_features))
                default_features = features.difference(unused_features)
                
                #enable at theta_init only default features
                for f in features:
                    if not f:
                        continue
                    if f in default_features:
                        fp.write("@1:F:features:%s {yes,no}[yes]\n" %(f))
                    else:
                        fp.write("@1:F:features:%s {yes,no}[no]\n" %(f))
                    
            if add_groups:
                feature_steps = metainfo.feature_group_dict.keys()
                default_steps = metainfo.feature_steps
                for s in feature_steps:
                    if s in default_steps:
                        fp.write("@1:F:feature-steps:%s {yes,no}[yes]\n" %(s))
                    else:
                        fp.write("@1:F:feature-steps:%s {yes,no}[no]\n" %(s))
                    
            if add_algorithms:
                algorithms = metainfo.algorithms
                for a in algorithms:
                    fp.write("@1:F:algorithms:%s {yes,no}[yes]\n" %(a))
            
            fp.write("@1:mem-limit {%d}[%d]" %(mem_limit, mem_limit))
                    
        return new_pcs
        
    def evaluate(self, conf, scen, fold, wrapper, cutoff, out_conf, out_def, default=True):
        '''
            evaluates <conf> configuration with <wrapper> on scenario <scen> with <fold> split as test set
        '''
        
        conf = conf.split(" ")
        scen_position = conf.index("-@1:coseal-dir")
        conf.pop(scen_position) # remove parameter -@1:coseal-dir
        conf.pop(scen_position) # and its value
        conf = " ".join(conf)
        
        cmd = "python %s config_wrapper.json %d 0 %d -1 -1 %s -@1:coseal-dir %s -@1:print-times %s" %(wrapper, fold, cutoff, conf, scen, out_conf)
        logging.info(cmd)
        if not os.path.isfile(out_conf):
            p = Popen(cmd, shell=True)
            p.communicate()
        else:
            logging.info("Found %s - not running claspfolio!" %(out_conf))
        
        conf_dict = self.read_csv(out_conf)
        
        def_dict = None
        if default:
            if not os.path.isfile(out_def):
                cmd = "python %s config_wrapper.json %d 0 %d -1 -1 %s -@1:coseal-dir %s -@1:print-times %s" %(wrapper, fold, cutoff, self.DEFAULT, scen, out_def)
                logging.info(cmd)
                p = Popen(cmd, shell=True)
                p.communicate()
            else:
                logging.info("Found %s - not running claspfolio!" %(out_def))
                
            def_dict = self.read_csv(out_def)
        
        return conf_dict, def_dict
        
    def read_csv(self, file_):
        perf_dict = {}
        with open(file_) as fp:
            csv_reader = csv.reader(fp)
            for row in csv_reader:
                inst_ = row[0]
                try:
                    perf = float(row[1])
                except:
                    continue
                perf_dict[inst_] = perf
        return perf_dict
        
        