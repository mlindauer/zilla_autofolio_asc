'''
Get performance of set of predictions according to ICON competition format
instance,runID,solver,timeLimit
'''
 

import csv
import sys
import os
import logging

sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))

from ZillaRunDataAnalyzer import *


def main():
    
    logging.basicConfig(level=logging.INFO)


    predictions_filename = sys.argv[1]
    
    run_data_filename = sys.argv[2]
    
    test_predictions(predictions_filename, run_data_filename)
    

def test_predictions(filename, run_data_filename):
    
    #zilla_folder = os.path.join(os.path.dirname(filename),'zilla-scenario')
    #run_data_filename = os.path.join(zilla_folder, 'algo-runs.csv')
    run_data = readZillaRunData([run_data_filename])
    unsolved_instances = getUnsolvedInstances(run_data)
    
    solved_instances = list(set(run_data.keys()) - set(unsolved_instances))
    
#     algorithms = []
#     for instance in unsolved_instances:
#         if not(len(algorithms) == 0 or rundata[instance].keys() == algorithms):
#             raise Exception('Instance '+instance+' has not been executed by the same algorithms ('+str(rundata[instance].keys())+') as the other instances ('+str(algorithms)+').')
#         algorithms = rundata[instance].keys()
    
    zilla_autofolio_data = transform_predictions(filename, run_data)
    vbs_data = getVBS(run_data,'vbs')
    sb_data = getSB(run_data,'sb')

    #print unsolved_instances
    
    #print zilla_autofolio_data.keys()

    zilla_autofolio = getMeanPARk(zilla_autofolio_data,['zilla_autofolio'],solved_instances,10)[0]
    vbs = getMeanPARk(vbs_data,['vbs'],solved_instances,10)[0]
    sb = getMeanPARk(sb_data,['sb'],solved_instances,10)[0]
    
    #(instancesSolved, PAR10) = self.getResults(results_file, run_data)
    
    logging.info('VBS Performance: %f', vbs)
    logging.info('Single Best Performance: %f', sb)
    logging.info('Zilla-Autofolio Performance: %f', zilla_autofolio)
    logging.info('Single Best - VBS gap reduced by: %f percent', 100 *((sb-zilla_autofolio) / (sb-vbs+0.01)))
    

def transform_predictions(filename, rundata):
    
    input_file = csv.DictReader(open(filename))
    predictions_data = {}
    for line in input_file:
        #print line
        instance = line['instanceID']
        
        #if solved[instance]:
        #    continue
        
        runID = line['runID']
        solver = line['solver']
        timeLimit = line['timeLimit']
        
        (result,runtime,quality,cutoff,seed) = rundata[instance][solver]

        if instance in predictions_data:
            instance_result = predictions_data[instance]['zilla_autofolio'][0]
            instance_runtime = predictions_data[instance]['zilla_autofolio'][1]
            if instance_result == 'SAT' or instance_result == 'UNSAT':
                continue
            else:
                if instance_runtime < cutoff -runtime:
                    instance_runtime == runtime
                    instance_result = result
                else:
                    instance_runtime = cutoff;
                    instance_result = 'TIMEOUT' 
                    
            predictions_data[instance]['zilla_autofolio'] = (instance_result, instance_runtime, quality, cutoff, seed)       
        else:
            if runtime < cutoff:
                instance_runtime = runtime
                instance_result = result
            else:
                instance_runtime = cutoff;
                instance_result = 'TIMEOUT'
                
            predictions_data[instance] = {}
            predictions_data[instance]['zilla_autofolio'] = (instance_result, instance_runtime, quality, cutoff, seed)

    return predictions_data

    
def getSB(alldata,SB):
    performance = {}
    for instance in alldata.keys():
        for algorithm in alldata[instance].keys():
            if algorithm not in performance.keys():
                performance[algorithm] = 0
            (result,runtime,quality,cutoff,seed) = alldata[instance][algorithm]
            if runtime >= cutoff:
                performance[algorithm] += cutoff*10
            else:
                performance[algorithm] += runtime
    
    singleBest = min(performance.iteritems(), key=operator.itemgetter(1))[0]           
    
    sum= 0.0
    single_best_rundata = {}       
    for instance in alldata.keys():
        single_best_rundata[instance] = {}
        (result,runtime,quality,cutoff,seed) = alldata[instance][singleBest]
        sum += runtime
        single_best_rundata[instance][SB] = (result,runtime,quality,cutoff,seed)
        
    #print 'Single Best Mean runtime is: ' + str(sum/len(rundata.keys()))
    
    return single_best_rundata

def getVBS(alldata, vbs):
    """
    Add Virtual Best Solver (VBS) data in the given run data. The VBS minimizes runtime and quality across
    all the available algorithms for a given instance.

    rundata - the rundata dictionary.
    """
    rundata = {}

    for instance in alldata.keys():
        
        rundata[instance] = {}
        
        vbsruntime = None
        vbsquality = None
        vbsresult = None
        for algorithm in alldata[instance].keys():
            (result,runtime,quality,cutoff,seed) = alldata[instance][algorithm]
            if vbsresult == None or vbsresult == 'CRASHED' or result == 'SAT' or result == 'UNSAT':
                vbsresult = result
            if vbsruntime == None or vbsruntime > runtime:
                vbsruntime = runtime
            if vbsquality == None or vbsquality > quality:
                vbsquality = quality

        if vbs in rundata[instance].keys():
            raise Exception('Algorithm VBS already in run data for instance '+instance+'.')

        rundata[instance][vbs] = (vbsresult,vbsruntime,vbsquality,cutoff,seed)
        
    return rundata
        
def getUnsolvedInstances(runresults):
    """
    Return a list of instances that are not solved by any algorithm.

    runresults - a run results dictionary taking problem instance to a dictionary taking algorithm to
    a tuple (runtime,runresult,cutoff).
        runresults[problem instance][algorithm] -> (runtime,runresult,cutoff)
    """
    unsolved_instances = []
    instances = runresults.keys()
    for instance in instances:
        unsolved = True
        for algorithm in runresults[instance].keys():
            (result,runtime,quality,cutoff,seed) = runresults[instance][algorithm]
            if result == 'SAT' or result == 'UNSAT':
                unsolved = False
                break
        if unsolved:
            unsolved_instances.append(instance)

    return unsolved_instances   
    
if __name__ == '__main__':
    main()    