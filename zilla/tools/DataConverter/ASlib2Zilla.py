"""
Data converter that transforms ASlib scenarios, algorithm runs and features data to Zilla-ready scenario folder with formatted data.

@author - Alexandre Frechette (afrechet@cs.ubc.ca)
@revision - 23/11/2014
"""

import sys
import os
import argparse
import logging
import shutil
import random
import math

#Zilla data converter.
from ZillaConverter import *

#COSEAL ASlib data reader.
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)),'COSEAL-reader'))
from coseal_reader import *

def init():
    """
    Reads and parses command-line argument and initializes script-wide logging and constants.
    """
    #Define argument parser.    
    parser = argparse.ArgumentParser(
                description=
                """Convert an ASlib scenario to a Zilla scenario, reformatting the data as necessary.""",
                epilog=
                """by Alexandre Frechette (afrechet@cs.ubc.ca)"""
            )
    #Scenario folder option.
    parser.add_argument('input_scenario_folder',metavar='ASlib input scenario folder', help='the ASlib scenario folder to convert')
    parser.add_argument('output_scenario_folder',metavar='zilla output scenario folder', help='the ASlib scenario folder to convert')

    #Log level
    def parseLogLevel(loglevel):
        numeric_level = getattr(logging, loglevel.upper(), None)
        if not isinstance(numeric_level, int):
            raise ValueError('Invalid log level: %s' % loglevel)
        return numeric_level
    parser.add_argument('--log-level',dest = 'loglevel', type=parseLogLevel,default=logging.INFO, help='the level of logging to display (default: %(default)s)')

    #Overwrite 
    parser.add_argument('--replace-output', dest = 'replace_output', type=bool, choices = [True,False], default=False, help='whether to replace the zilla scenario output folder if it already exists (default: %(default)s)')

    #Parse arguments
    args = parser.parse_args()

    #Validate arguments
    if not os.path.isdir(args.input_scenario_folder):
        raise ValueError('ASlib input scenario folder "%s" does not exist.' % scenario_folder)
    if os.path.exists(args.output_scenario_folder):
        if args.replace_output:
            shutil.rmtree(args.output_scenario_folder)
        else:
            raise ValueError('Zilla output scenario folder "%s" already exists, cannot overwrite.' % args.output_scenario_folder)

    #Initialize logging
    logging.basicConfig(format='[%(levelname)s] %(message)s',level=args.loglevel)

    return args

class ASlibData(object):
    """
    Contains necessary ASlib scenario data.
    """

    instance_dict = {}          #Dictionary of ASlib instance names to ASlib instances.
    instances = []              #List of instances.
    algorithms = []             #List of algorithms
    scenario_name = ""          #ASlib scenario name.
    algo_cutoff = -1            #ASlib algorithm cutoff time.
    features_cutoff = -1        #ASlib feature extraction cutoff.
    ground_truths_names = []    #List of ASlib instances ground truths attribute names.
    features_names_indices = {} #Dictionary of feature names to their attribute index.
    features_names = []         #List of all feature names.
    cheap_features_step = ""    #Name of the cheap features step.
    cheap_features_names = []   #List of cheap feature names.
    folds = {}                  #Dictionary of instance to (fold,repetition) tuple.

    def __init__(self,scenario_folder):
        """
        Reads in an ASlibData instance from an ASlib scenario folder.

        scenario_folder - an ASlib scenario folder. 
        """

        #Get ASlib data.
        logging.info('Reading in ASlib scenario using COSEAL reader...')
        aslib_reader = CosealReader()
        parser = argparse.ArgumentParser()
        dummy_coseal_reader_args = parser.parse_args([])
        dummy_coseal_reader_args.feat_time = -1
        dummy_coseal_reader_args.feature_steps = None 
        self.instance_dict, metainfo, algo_dict = aslib_reader.parse_coseal(scenario_folder,dummy_coseal_reader_args)    
        
        #Transform data in required form
        self.instances = self.instance_dict.keys()
        self.algorithms = algo_dict.keys()
            
        #Input and validate metainfo.
        self.scenario_name = metainfo.scenario
        if self.scenario_name == None:
            raise ValueError('There is no scenario name.')
        if 'runtime' not in metainfo.performance_measure:
            raise ValueError("Runtime is not one of the scenario's performance measure, and zilla only works with runtime.")
        if len(metainfo.performance_measure) > 1:
            logging.warning('The scenario has more than one performance measure, but only runtime will be used in the translated zilla scenario.')
        if 'runtime' not in metainfo.performance_type:
            raise ValueError("Runtime is not one of the scenario's performance type, and zilla only works with runtime.")
        if len(metainfo.performance_type) > 1:
            logging.warning('The scenario has more than one performance type, but only runtime will be used in the translated zilla scenario.')
        if type(metainfo.maximize) is list:
            if metainfo.maximize[metainfo.performance_measure.index('runtime')].lower() != 'false':
                raise ValueError('Zilla cannot maximize runtime.')
        elif metainfo.maximize.lower() != 'false':
            raise ValueError('Zilla cannot maximize runtime.')
        self.ground_truths_names = metainfo.ground_truths
        if 'satunsat' not in self.ground_truths_names:
            logging.warning('No SATUNSAT ground truth - all solved instances will be considered SAT.')    
        self.algo_cutoff = metainfo.algorithm_cutoff_time
        if self.algo_cutoff == None:
            raise ValueError('There is no algorithm cutoff time.')
        if metainfo.algorithm_cutoff_memory not in [None, '?']:
            logging.warning('Zilla cannot enforce an algorithm cutoff memory.')
        self.features_cutoff = metainfo.features_cutoff_time
        if self.features_cutoff == None:
            raise ValueError('There is no feature cutoff time.')
        if metainfo.features_cutoff_memory not in [None, '?']:
            logging.warning('Zilla cannotenforce a features cutoff memory.')
        if metainfo.feature_steps == None:
            raise ValueError('There were no provided feature extraction steps.') 
        if metainfo.features == None:
            raise ValueError('No features are available.')
        self.features_names_indices = {metainfo.features[i]:i for i in range(len(metainfo.features))}

        feature_group_dict = metainfo.feature_group_dict        
        #Some weirdness because feature steps are saved unintuitively by ASlib reader.
        self.cheap_features_step = metainfo.feature_steps[0]
        unused_features = set()
        unused_steps = set(feature_group_dict.keys()) - set([self.cheap_features_step])
        for u_step in unused_steps:
            not_processed_features = feature_group_dict[u_step] 
            unused_features = unused_features.union(set(not_processed_features))
        self.cheap_features_names = set(metainfo.features).difference(unused_features)
        if len(self.cheap_features_names) == 0:
            raise ValueError('No cheap features to use corresponding to step %s.' % self.cheap_features_step)        

        logging.debug('Cheap features: %s.',','.join(self.cheap_features_names))
        logging.warning('Cheap features (%s) are assumed to be computable in zero time.', ','.join(self.cheap_features_names))    
        self.features_names = []
        for feature_group in metainfo.feature_steps:
            self.features_names.extend(feature_group_dict[feature_group])    
        logging.debug('Features: %s.',','.join(self.features_names))
        
        for instance in self.instances:
            coseal_instance = self.instance_dict[instance]
            for r in coseal_instance._fold:
                f=coseal_instance._fold[r]
                self.folds[instance] = (f,r)

def convert(aslib_data):
    """
    Convert an ASlib data instance to a triple of zilla-formatted features and algorithm run results.

    aslib_data - an ASlib data instance.
    """

    #Construct algorithm run results and feature run results.
    algo_run_results = dict()
    cheap_features_run_results = dict()
    features_run_results = dict()

    for instance in aslib_data.instances:
        logging.debug('Processing instance %s ...',instance)
        if instance not in aslib_data.instance_dict:
            raise Exception('Instance %s is in instance set, but no ASlib data is available.' % instance)

        aslib_instance = aslib_data.instance_dict[instance]

        #Get features
        if instance in cheap_features_run_results or instance in features_run_results:
            raise Exception('Duplicate instance %s in instance set.' % instance) 
        cheap_features_run_results[instance] = dict()
        features_run_results[instance] = dict()

        features_values = aslib_instance._features
        
        seed = 0
        cheap_features = {}
        features = {}
        if features_values == None:
            result = 'CRASHED'
            runtime = 0
            logging.error("No ASlib feature values for instance %s. Instance won't have features in zilla scenario.", instance)
             
        else:
            result = 'SAT'
            
            try:
                cheap_runtime = float(aslib_instance._feature_group_cost_dict[aslib_data.cheap_features_step])
            except Exception as e:
                logging.warning('Could not parse instance %s cheap feature cost - assuming 0.' % instance)
                cheap_runtime = 0

            try:
                total_runtime = float(aslib_instance._feature_cost_total)
            except Exception as e:
                logging.error('Could not parse instance %s total feature cost.' % instance)
                raise e
            for f in aslib_data.cheap_features_names:
                i=aslib_data.features_names_indices[f]
                try:
                    v = float(features_values[i])
                except Exception as e:
                    logging.debug('Could not cast %s feature value (%s) to float - assuming incomplete.',f,features_values[i])
                    v = -512
                cheap_features[f] = v
            for f in aslib_data.features_names:
                i=aslib_data.features_names_indices[f]
                try:
                    v = float(features_values[i])
                except Exception as e:
                    logging.debug('Could not cast %s feature value (%s) to float - assuming incomplete.',f,features_values[i])
                    v = -512
                features[f] = v
        
        cheap_features_additional_run_data = ';'.join(['%s=%s' % (f,str(cheap_features[f])) for f in cheap_features])     
        cheap_features_run_results[instance]['ASlib-features'] = (result,str(cheap_runtime),str(cheap_runtime),str(aslib_data.features_cutoff),seed,cheap_features_additional_run_data)
          
        features_additional_run_data = ';'.join(['%s=%s' % (f,str(features[f])) for f in features])     
        features_run_results[instance]['ASlib-features'] = (result,str(total_runtime),str(total_runtime),str(aslib_data.features_cutoff),seed,features_additional_run_data)

        #Get algorithm executions        
        if instance in algo_run_results:
            raise Exception('Duplicate instance %s in instance set.' % instance) 
        algo_run_results[instance] = dict()
        for algorithm in aslib_data.algorithms:
            
            status = aslib_instance._status[algorithm]
            if 'satunsat' in aslib_data.ground_truths_names: 
                answer = aslib_data.ground_truths_names['satunsat'][aslib_instance._ground_truth['satunsat']].upper()
                if answer not in ['SAT','UNSAT']:
                    raise ValueError('Unexpected SATUNSAT ground truth value for instance %s: %s.' % (instance,answer))
            else:
                answer = 'SAT'

            if status == 'ok':
                result = answer
            elif status == 'timeout':
                result = 'TIMEOUT' 
            else:
                logging.warning('Algorithm %s on instance %s status that is not "ok" or "timeout", setting result to CRASHED.',algorithm,instance)
                result = 'CRASHED'

            runtime = aslib_instance._cost['runtime'][algorithm]
            if len(runtime) > 1:
                logging.warning('Runtime of algorithm %s on instance %s has been measured multiple times, only taking the first occurence.' % (instance,algorithm))
            runtime = runtime[0]
            quality = runtime
            cutoff = aslib_data.algo_cutoff  
            seed = 0
            additional_run_data = 'ASlib run'
            
            algo_run_results[instance][algorithm] = (str(result),str(runtime),str(quality),str(cutoff),str(seed),str(additional_run_data))

    return (cheap_features_run_results,features_run_results,algo_run_results)

def writeZillaParameterFiles(aslib_data,output_folder):
    """
    Write the zilla scenario parameter files in the output folder.
    """
    folds = list(set([f for (f,r) in aslib_data.folds.values()]))

    for fold in folds:
        logging.debug('Creating scenario for test fold %d ...' % fold)
        logging.debug('Creating training and validation instance sets ...')
        train_instances = []
        test_instances = []
        for instance in aslib_data.folds:
            (f,r) = aslib_data.folds[instance]
            if f == fold:
                for i in range(r):
                    test_instances.append(instance)
            else:
                for i in range(r):
                    train_instances.append(instance)

        random.shuffle(train_instances)
        test_instances.sort()

        logging.debug('Doing a 60%-40% training/validation split of all instances ...')
        n = int(math.ceil(len(train_instances)*0.6))
        training_instances = sorted(train_instances[:n])
        validation_instances = sorted(train_instances[n:])
        zilla_traininstances_filename = os.path.join(output_folder,'training-instances_f%d.csv' % fold)
        logging.debug('Writing zilla training instances to "%s" ...',zilla_traininstances_filename)
        with open(zilla_traininstances_filename,'wb') as zilla_traininstances_file:
            for instance in training_instances:
                zilla_traininstances_file.write(instance+'\n')
        zilla_validinstances_filename = os.path.join(output_folder,'validation-instances_f%d.csv' % fold)
        logging.debug('Writing zilla validation instances to "%s" ...',zilla_validinstances_filename)
        with open(zilla_validinstances_filename,'wb') as zilla_validinstances_file:
            for instance in validation_instances:
                zilla_validinstances_file.write(instance+'\n')
        zilla_testinstances_filename = os.path.join(output_folder,'testing-instances_f%d.csv' % fold)
        with open(zilla_testinstances_filename,'wb') as zilla_testinstances_file:
            for instance in test_instances:
                zilla_testinstances_file.write(instance+'\n')

        zilla_scenario_filename = os.path.join(output_folder,'scenario.txt')
        logging.debug('Modifying generic scenario at "%s" ...',zilla_scenario_filename)
        with open(zilla_scenario_filename,'rb') as zilla_scenario_file:
            content = zilla_scenario_file.read()
            if '%SCENARIO_NAME' not in content:
                logging.warning('No scenario name to replace in generic zilla scenario.')
            else:
                content = content.replace('%SCENARIO_NAME',aslib_data.scenario_name+'_f%d' % fold)
            if '%CUTOFF' not in content:
                logging.warning('No runtime cutoff to replace in generic zilla scenario.')
            else:
                content = content.replace('%CUTOFF',str(aslib_data.algo_cutoff)) 
            if '%TRAIN_INSTANCES' not in content:
                logging.warning('No train instances name to replace in generic zilla scenario.')
            else:
                content = content.replace('%TRAIN_INSTANCES',os.path.split(zilla_traininstances_filename)[1])
            if '%VALIDATION_INSTANCES' not in content:
                logging.warning('No validation instances name to replace in generic zilla scenario.')
            else:
                content = content.replace('%VALIDATION_INSTANCES',os.path.split(zilla_validinstances_filename)[1])
            if '%TEST_INSTANCES' not in content:
                logging.warning('No test instances name to replace in generic zilla scenario.')
            else:
                content = content.replace('%TEST_INSTANCES',os.path.split(zilla_testinstances_filename)[1])
             
        zilla_scenario_filename = os.path.splitext(zilla_scenario_filename)[0]+'_f%d'%fold+os.path.splitext(zilla_scenario_filename)[1]
        with open(zilla_scenario_filename,'wb') as zilla_scenario_file:
            zilla_scenario_file.write(content)

    zilla_compfeaturemanager_filename = os.path.join(output_folder,'complete-feature-manager.txt') 
    logging.debug('Modifying generic complete feature manager at "%s" ...', zilla_compfeaturemanager_filename)
    with open(zilla_compfeaturemanager_filename,'rb') as zilla_compfeaturemanager_file:
        content = zilla_compfeaturemanager_file.read()
        if '%FEATURE_CUTOFF' not in content:
            logging.warning('No feature cutoff to replace in generic zilla complete feature manager.')
        else:
            content = content.replace('%FEATURE_CUTOFF',str(aslib_data.features_cutoff))    
    with open(zilla_compfeaturemanager_filename,'wb') as zilla_compfeaturemanager_file:
        zilla_compfeaturemanager_file.write(content)
    

def main():
    #Initialize script and parse arguments.
    args = init()
    
    #Read in necessary arguments
    scenario_folder = args.input_scenario_folder
    output_folder = args.output_scenario_folder
    
    #Create zilla scenario folder.
    generic_zilla_scenario_folder = os.path.join(os.path.dirname(os.path.realpath(__file__)),'zilla-generic-scenario')
    if not os.path.isdir(generic_zilla_scenario_folder):
        raise Exception('Could not find generic zilla scenario folder at "%s".' % generic_zilla_scenario_folder)
    shutil.copytree(generic_zilla_scenario_folder,output_folder)

    #Read in ASlib data.
    logging.info('='*80)
    logging.info('Reading in data from ASlib scenario located at "%s"...', scenario_folder)
    aslib_data = ASlibData(scenario_folder)

    #Convert ASlib data to zilla run results.
    logging.info('='*80)
    logging.info('Converting ASlib scenario data to zilla data ...')
    (cheap_features_run_results,features_run_results,algo_run_results) = convert(aslib_data)

    #Write zilla run results to output folder.
    logging.info('='*80)
    cheap_features_filename = os.path.join(output_folder,'cheap-features.csv')
    logging.info('Writing zilla cheap features to "%s" ...',cheap_features_filename)
    writeZillaFormatFeatures(cheap_features_run_results,cheap_features_filename)
    
    features_filename = os.path.join(output_folder,'features.csv')
    logging.info('Writing zilla features to "%s" ...', features_filename)
    writeZillaFormatFeatures(features_run_results,features_filename)

    algo_runs_filename = os.path.join(output_folder,'algo-runs.csv')
    logging.info('Writing algorithm run results to "%s" ...', algo_runs_filename)
    writeZillaFormatRunResult(algo_run_results,algo_runs_filename)
    
    #Modify generic zilla files.
    logging.info('='*80)
    logging.info('Modifying the generic zilla parameter files and scenario at "%s" ...',output_folder)
    writeZillaParameterFiles(aslib_data,output_folder)
    
    logging.info('='*80)
    logging.info('... done!')
    logging.info('Zilla scenario written in "%s".',output_folder)

if __name__ == '__main__':
    main()