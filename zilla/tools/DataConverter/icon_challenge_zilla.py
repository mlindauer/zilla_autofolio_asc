'''
ICON Challenge on Algorithm Selection: Zilla Submission
Interface for calling zilla adhering to the ICON Challenge on Algorithm selection

Author : Chris Cameron
'''

import sys
import os
import argparse
import logging
import shutil
import random
import math
import random
from subprocess import call
    

#Zilla data converter.
from ZillaConverter import *

import Zilla
from Zilla import *

#COSEAL ASlib data reader.
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)),'COSEAL-reader'))
from coseal_reader import *

def main():
    
    zilla_release = sys.argv[1]
    ASlib_scenario = sys.argv[2]
    zilla_folder = sys.argv[3] + str(random.randint(1,100000))
    feature_groups=['Pre'] # Leave as None if unrestricted
    num_folds = 10
    
    zilla = Zilla(zilla_release,ASlib_scenario, zilla_folder, feature_groups, 'clasp2', '1.0', num_folds)
    
    zilla.get_performance_estimate()
    #model = zilla.train()
    #file = zilla.test(zilla.zilla_folder)
    print 'Output file printed to: %s' % file
    print 'Shutting down...'
    return

if __name__ == '__main__':
    main()
    