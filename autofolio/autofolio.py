#!/usr/local/bin/python2.7
# encoding: utf-8
'''
autofolio -- algorithm configuration for algorithm selection

@author:     Marius Lindauer    

@copyright:  2015 Marius Lindauer. All rights reserved.

@license:    GPLv3

@contact:    lindauer@cs.uni-freiburg.de    
'''

import sys
import os
import logging
import traceback
import random
import inspect

from argparse import ArgumentParser
from argparse import ArgumentDefaultsHelpFormatter

cmd_folder = os.path.realpath(os.path.abspath(os.path.split(inspect.getfile( inspect.currentframe() ))[0]))
if cmd_folder not in sys.path:
    sys.path.append(cmd_folder)
cmd_folder = os.path.realpath(os.path.join(cmd_folder, ".."))
if cmd_folder not in sys.path:
    sys.path.append(cmd_folder)


from aslib.aslib_parser import ASlibParser

from selector.claspfolio import ClaspfolioSpecifics
from configurator_interfaces.smac import SMAC
from job_systems.local import LocalExecutor

from wrappers.flexfolio.parameter_parser import ParameterParser

__all__ = []
__version__ = 0.1
__date__ = '2015-07-02'
__updated__ = '2015-07-02'

class Autofolio(object):
    
    def __init__(self, root_path, args):
        ''' Constructor '''
        self.root_path = root_path
        
        self.scenario = args.scenario
        
        #Configuration
        self.budget = args.budget
        self.cutoff = args.cutoff
        self.reps = args.repetitions
        self.wrapper = args.wrapper
        if not os.path.isfile(self.wrapper):
            self.wrapper = os.path.join(root_path, self.wrapper)
            if not os.path.isfile(self.wrapper):
                logging.error("Have not found target algorithm wrapper (--wrapper): %s" %(args.wrapper))
        self.configurator = args.configurator
        
        # PCS
        self.selector = args.selector
        self.pcs = args.pcs
        
        # MISC
        self.tmp = args.tmp
        self.runsolver = args.runsolver
        
        self.basename_scenario = None
        
        self._INNER_FOLDS = 10
        self.MEMLIMIT = 2048
        
    def main(self):
        ''' main method '''
        as_parser = ASlibParser(self.scenario, self.tmp)
        self.scenario = as_parser.get_scenario() # in case we had to write a new cv.arff file
        metainfo = as_parser.get_metainfo()
        pcs = self.add_parameters_to_pcs(self.scenario, metainfo, self.tmp)
        calls = self.write_conf_scenarios(pcs, self.tmp)
        self.submit_job(calls)
        
        smac = SMAC()
        conf, perf = smac.get_best_conf("autofolio", self.tmp)
        print(conf)

        pp = ParameterParser()
        print(conf.split(" "))
        parsed_params = pp.parse_parameters(conf.split(" "),"--")[0][1]
        # remove unnecessary meta params
        as_head = parsed_params.index("--aslib-dir")
        parsed_params.pop(as_head)
        parsed_params.pop(as_head)
        mem_head = parsed_params.index("--mem-limit")
        parsed_params.pop(mem_head)
        parsed_params.pop(mem_head)
        
        return " ".join(parsed_params)
                       
    def add_parameters_to_pcs(self, scenario, metainfo, output_dir):
        '''
            add parameters to pcs
        '''
        logging.debug("Write extended pcs file")
        out_pcs = os.path.join(output_dir, "autofolio.pcs")
        if self.selector == "flexfolio":
            cf_specs = ClaspfolioSpecifics()
            cf_specs.extend_pcs(self.pcs, scenario, metainfo, out_pcs, mem_limit=self.MEMLIMIT)
        return out_pcs
        
    def write_conf_scenarios(self, pcs, outdir):
        '''
            write the configuration scenarios
        '''
        logging.debug("Write SMAC scenarios")
        if self.configurator == "SMAC":
            smac = SMAC(self.root_path, self.reps, self.budget, self.cutoff, self.wrapper, self.tmp, self.runsolver)
            calls = smac.get_ac_calls([pcs], self._INNER_FOLDS, outdir)
            
        for c in calls:
            logging.debug(c)
        return calls
    
    def submit_job(self, calls):
        '''
            based on <clustersystem> submit jobs (or execute it locally) 
        '''
        runner = LocalExecutor()
        runner.submit_jobs(calls)

def ext_func(scenario_dir, budget, outdir):
    '''
        run AutoFolio on scenario <scenario_dir> with configuration budget <budget> and write all files to <outdir>
    '''
    return main(argv=["-s", scenario_dir, "-b", str(budget), "-t", outdir])
    
    

def main(argv=None): # IGNORE:C0111
    '''Command line options.'''

    #program_name = os.path.basename(sys.argv[0])
    program_version = "v%s" % __version__
    program_build_date = str(__updated__)
    program_version_message = '%%(prog)s %s (%s)' % (program_version, program_build_date)
    program_shortdesc = __import__('__main__').__doc__.split("\n")[1]
    program_license = '''%s

  Created by Marius Lindauer on %s.
  Copyright 2015. All rights reserved.

  Licensed under the GPLv3
  http://www.gnu.org/copyleft/gpl.html

  Distributed on an "AS IS" basis without warranties
  or conditions of any kind, either express or implied.

USAGE
''' % (program_shortdesc, str(__date__))

    try:
        
        if argv:
            root_path = os.path.join(os.path.split(sys.argv[0])[0])
        else:
            root_path = os.path.join(os.path.split(sys.argv[0])[0], "..")
            
        if root_path == "":
            root_path = "."
            
        # Setup argument parser
        parser = ArgumentParser(description=program_license, formatter_class=ArgumentDefaultsHelpFormatter)
        
        req_group = parser.add_argument_group('Required Options')
        req_group.add_argument("-s", '--scenario', required=True, help="aslib scenario")
        #parser.add_argument('--selector', default="claspfolio", choices=["claspfolio"], help="algorithm selector to configure")
        
        conf_group = parser.add_argument_group('Configuration Options')
        conf_group.add_argument("-b", '--budget', default=7200, type=int, help="configuration budget")
        conf_group.add_argument("-c", '--cutoff', default=1800, type=int, help="target algorithm selector runtime cutoff")
        conf_group.add_argument("-r", '--repetitions', type=int, default=1, help="number of configuration runs per split")
        conf_group.add_argument('--wrapper', default="wrappers/flexfolio/ff_wrapper.py", help="wrapper of algorithm selector.py")
        conf_group.add_argument('--selector', default="flexfolio", choices=["flexfolio"], help="selector to configure - has to compatible with wrapper")
        conf_group.add_argument('--configurator', default="SMAC", choices=["SMAC"], help="call of configurator (only SMAC is supported)")
        
        pcs_group = parser.add_argument_group('PCS Options')
        pcs_group.add_argument('--pcs', default=os.path.join(root_path, "pcs/claspfolio-2.0.0-classvoter.txt"), help="psc file of selector")
        
        misc_group = parser.add_argument_group('MISC Options')
        misc_group.add_argument("-v", "--verbose", dest="verbose", default="INFO", choices=["INFO", "DEBUG","NOTEST"], help="set verbosity level [default: %(default)s]")
        misc_group.add_argument("-V", "--version", action='version', version=program_version_message)
        misc_group.add_argument("-S", '--seed', type=int, default=12345, help="random seed")
        misc_group.add_argument("-t", '--tmp', default=".", help="directory for temporary files")
        misc_group.add_argument("-R", '--runsolver', default=os.path.join(root_path, "runsolver", "runsolver"), help="runsolver to limit the runtime of target algorithm")
        
        # Process arguments
        if argv: 
            args = parser.parse_args(argv)
        else:
            args = parser.parse_args()
        logging.basicConfig(format="[%(levelname)s]: %(message)s", level=args.verbose)
        random.seed(args.seed)
        
        autofolio = Autofolio(root_path, args)
        return autofolio.main()
        
    except KeyboardInterrupt:
        ### handle keyboard interrupt ###
        return 0
    except Exception, e:
        traceback.print_exc()
        return 1

if __name__ == "__main__":
    sys.exit(main())