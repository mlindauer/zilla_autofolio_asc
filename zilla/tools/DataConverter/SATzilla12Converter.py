import DataConverter
from DataConverter import *

def readSATzilla12Features(input_filename):
    """
    Converts a list of files in SATzilla features format to a run result dictionary.

    input_filenames - a list of MIPzilla rundata  formatted files.
    """

    runresults = dict()

    print 'Reading data from "'+input_filename+'"...'
    features = []
    algorithm = input_filename.split(os.sep)[-1]
    with open(input_filename,'rb') as csvfile:
        csvreader = csv.reader(csvfile)
        #Process the headers.
        infoheader = csvreader.next()

        if not infoheader[0] == '<INFO>':
            raise Exception('Missing <INFO> header at the top of the file providing cutoff.')
        else:
            if not  infoheader[1].split('=')[0].lower() == 'cutoff':
                raise Exception('Missing cutoff value in <INFO> header.')
            else:
                cutoff = infoheader[1].split('=')[1]

        header = csvreader.next()
        for i in range(1,len(header)):
            features.append(header[i])

        featuretimeindices = filter(lambda i : 'time' in features[i-1],range(1,len(header)))

        for row in csvreader:
            instance = row[0].strip()

            if instance in runresults:
                raise Exception('Instance %s appears twice in provided files.' % instance)
            else:
                runresults[instance]=dict()

            if not len(row) == len(header):
                raise Exception('Row not the same length (%d) has header (%d) :\n %s' % (len(row),len(header),str(row)))
            runtime = str(sum([max(float(row[i]),0) for i in featuretimeindices]))
            additional_rundata = ';'.join([f.strip()+'='+v.strip() for (f,v) in zip(features,row[1:])])

            seed = DataConverter.ZF_INCOMPLETE_SEED
            quality = DataConverter.ZF_INCOMPLETE_QUALITY_VALUE
            result = 'SAT'

            runresults[instance][algorithm] = (result,runtime,quality,cutoff,seed,additional_rundata)

    return runresults

def __readSATzillaRunResults__(input_filenames,runtimeindex,solvedindex,qualityindex):
    runresults = dict()
    for input_filename in input_filenames:
        print 'Reading data from "'+input_filename+'"...'
        algorithms = []
        with open(input_filename,'rb') as csvfile:
            csvreader = csv.reader(csvfile)

            #Process the headers
            infoheader = csvreader.next()

            if not infoheader[0] == '<INFO>':
                raise Exception('Missing <INFO> header at the top of the file providing cutoff time.')
            else:
                if not  infoheader[1].split('=')[0].lower() == 'cutoff':
                    raise Exception('Missing cutoff value in <INFO> header.')
                else:
                    cutoff = infoheader[1].split('=')[1]

            header = csvreader.next()

            i=0
            while 5*i+1 < len(header):

                algorithm_entry = header[5*i+1]
                if '_' in algorithm_entry:
                    algorithm = '-'.join(algorithm_entry.split('_')[:-1]).strip()
                else:
                    algorithm = algorithm_entry.strip()

                if algorithm == '':
                    raise Exception('Could not parse algorithm name properly from %s - got %s .' % (algorithm_entry, algorithm))

                algorithms.append(algorithm)
                i+=1

            for row in csvreader:
                instance = row[0].strip()

                if instance in runresults:
                    print '[WARNING] Instance %s appears twice in provided files.' % instance
                else:
                    runresults[instance]=dict()

                i=0
                while 5*i+1< len(header):
                    algorithm = algorithms[i]

                    tempinstance = row[5*i]
                    if not tempinstance == instance:
                        raise Exception('Two different instances %s and %s appear on the same row.' % (instance,tempinstance))

                    if algorithm in runresults[instance].keys():
                        raise Exception('There are at least two occurence of run results for algorithm %s on instance %s.' % (algorithm,instance))

                    runtime = row[5*i+runtimeindex]

                    quality = row[5*i+qualityindex]
                    if quality == '999999':
                        quality = DataConverter.ZF_INCOMPLETE_QUALITY_VALUE

                    solved = row[5*i+solvedindex]
                    if solved == '0':
                        result = 'TIMEOUT'
                    elif solved == '1':
                        result = 'SAT'
                    elif solved == '2':
                        result = 'UNSAT'
                    else:
                        raise Exception('Unrecognized solved code %s for algorithm %s on instance %s.' % (solved,algorithm,instance))
                    #if result == 'TIMEOUT' and float(runtime)<float(cutoff):
                        #print '[WARNING] Algorithm %s on instance %s reported TIMEOUT but its runtime %s is less than the provided cutoff %s.' % (algorithm,instance,runtime,cutoff)
                    #if result == 'TIMEOUT' and not quality == DataConverter.ZF_INCOMPLETE_QUALITY_VALUE:
                        #print '[WARNING] Algorithm %s on instance %s reported TIMEOUT but its quality has a known values %s.' % (algorithm,instance,quality)

                    seed = DataConverter.ZF_INCOMPLETE_SEED
                    additional_rundata=""

                    runresults[instance][algorithm] = (result,runtime,quality,cutoff,seed,additional_rundata)

                    i+=1

    return runresults

def readSATzilla12v1RunResults(input_filenames):
    """
    Converts a list of files in SATzilla2012v2 rundata format to a run result dictionary.
    This format is the standard SATzilla2012 format but with quality as second entry and solved
    as the third entry of solver columns.

    input_filenames - a list of SATzilla2012 rundata  formatted files.
    """

    return __readSATzillaRunResults__(input_filenames,1,3,2)

    return runresults
def readSATzilla12v2RunResults(input_filenames):
    """
    Converts a list of files in SATzilla2012v2 rundata format to a run result dictionary.
    This format is the standard SATzilla2012 format but with quality as third entry and solved
    as the second entry of solver columns.

    input_filenames - a list of SATzilla2012 rundata  formatted files.
    """

    return __readSATzillaRunResults__(input_filenames,1,2,3)
