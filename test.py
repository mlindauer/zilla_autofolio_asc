#!/usr/local/bin/python2.7
# encoding: utf-8
'''
ZillaAutofolio -- Zilla and Autofolio entering the AS Challenge 2015

@author:     Marius Lindauer, Chris Cameron

@copyright:  2015 Marius Lindauer. All rights reserved.

@license:    GPLv3

@contact:    lindauer@cs.uni-freiburg.de    
'''

import sys
import os
import logging
import traceback
import random
import shutil
import arff # liac-arff
import time

from argparse import ArgumentParser, REMAINDER
from argparse import ArgumentDefaultsHelpFormatter

from autofolio.autofolio import ext_func

#sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
from zilla.tools.DataConverter.Zilla import *
#from Zilla import *


from subprocess import Popen, PIPE

__all__ = []
__version__ = 0.1
__date__ = '2015-07-02'
__updated__ = '2015-07-02'

class ZillaAutofolioTest(object):
    
    def __init__(self, root_path, args):
        ''' Constructor '''
        self.root_path = root_path
        self.scenario = args.scenario
        self.output_dir = args.output
        self.model_dir = args.model
        
        '''
        Zilla scenario construction
        '''
        zilla_release   = os.path.abspath(os.path.join(self.root_path, 'zilla'))
        zilla_output_folder = os.path.abspath(os.path.join(self.output_dir, 'zilla-scenario'))
        feature_groups = None # ['Pre']
        
        self.zilla = Zilla(zilla_release, self.scenario, zilla_output_folder, feature_groups, '', '', 2)
        
        self.presolver = args.presolver
        self.presolver_runtime = args.time_presolver
        
    def main(self):
        ''' main method '''
        if "config.json" in os.listdir(self.model_dir):
            logging.info("Flexfolio was trained and will be used for the predictions")
            self.run_flexfolio()
        else:
            logging.info("SATzilla was trained and will be used for the predictions")
            model_dir = os.path.join(self.model_dir, 'zilla-scenario')
            self.zilla.test(model_dir)
        
    def run_flexfolio(self):
        '''
            train flexfolio with <conf> for the AS Challenge 
        '''
        base_cmd = "python %s --aslib %s -C %s --output %s " %(os.path.join(self.root_path, "selectors", "flexfolio", "src", "flexfolio_run_aschallenge.py"),
                                                                  self.scenario,
                                                                  os.path.join(self.model_dir, "config.json"),
                                                                  os.path.join(self.output_dir, "predictions.csv"),
                                                                  )
        
        if self.presolver:
            base_cmd += "--pre-solver %s,%s " %(self.presolver, self.presolver_runtime)
    
        logging.info("Calling: %s" %(base_cmd))
        
        p = Popen(base_cmd, shell=True)
        p.communicate()
    

def main(argv=None): # IGNORE:C0111
    '''Command line options.'''

    if argv is None:
        argv = sys.argv
    else:
        sys.argv.extend(argv)

    #program_name = os.path.basename(sys.argv[0])
    program_version = "v%s" % __version__
    program_build_date = str(__updated__)
    program_version_message = '%%(prog)s %s (%s)' % (program_version, program_build_date)
    program_shortdesc = __import__('__main__').__doc__.split("\n")[1]
    program_license = '''%s

  Created by Marius Lindauer on %s.
  Copyright 2015. All rights reserved.

  Licensed under the GPLv3
  http://www.gnu.org/copyleft/gpl.html

  Distributed on an "AS IS" basis without warranties
  or conditions of any kind, either express or implied.

USAGE
''' % (program_shortdesc, str(__date__))

    try:
        
        root_path = os.path.join(os.path.split(sys.argv[0])[0])
        # Setup argument parser
        parser = ArgumentParser(description=program_license, formatter_class=ArgumentDefaultsHelpFormatter)
        
        parser.add_argument("-s", '--scenario', required=True, help="ASlib scenario with test data")
        parser.add_argument("-m", '--model', required=True, help="trained model data")
        parser.add_argument("-o", '--output', required=True, help="output for model files")
        parser.add_argument("-v", '--verbose', choices=["INFO", "DEBUG"], default="INFO", help="verbose level")
        parser.add_argument("-p", '--presolver', required=False, default=None, help="pre-solver")
        parser.add_argument("-t", '--time_presolver', required=False, default=None, help="time for pre-solver")
        
        
        # Process arguments
        args = parser.parse_args()
        logging.basicConfig(format="[%(levelname)s]: %(message)s", level=args.verbose)
        
        za = ZillaAutofolioTest(root_path, args)
        za.main()
        
    except KeyboardInterrupt:
        ### handle keyboard interrupt ###
        return 0
    except Exception, e:
        traceback.print_exc()
        return 1

if __name__ == "__main__":
    sys.exit(main())