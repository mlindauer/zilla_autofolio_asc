import DataConverter
from DataConverter import *

def readZillaFormatRunResults(input_filenames):
    """
    Read in a list of Zilla run data files as a run data dictionary (taking instances to algorithm to run result data).

    input_filenames - a list of Zilla run data filenames.
    """
    rundata = dict()

    for input_filename in input_filenames:
        print 'Reading data from',input_filename,'...'
        with open(input_filename,'rb') as csvfile:
            csvreader = csv.reader(csvfile,delimiter=',',quotechar='"')

            #Read the header for algorithm names.
            header = csvreader.next()

            algorithms = [h.strip() for h in header[1:]]

            for line in csvreader:
                instance = line[0]

                if not len(header) == len(line):
                    raise Exception('A line in Zilla run data file '+input_filename+' has not the same length as the header.')

                if instance not in rundata.keys():
                    rundata[instance] = dict()

                runresults = line[1:]
                for i in range(len(algorithms)):
                    algorithm = algorithms[i]
                    runresult = runresults[i]

                    (result,runtime,quality,cutoff,seed) = runresult.split(':')
                    additional_rundata = ''

                    if algorithm in rundata[instance].keys():
                        raise Exception('Run data for algorithm '+algorithm+' on instance '+instance+' appears twice.')

                    #Changed crashed runs runtime to cutoff.
                    #if result == 'CRASHED':
                        #runtime = cutoff

                    #Check validity of runtime when TIMEOUT
                    if result == 'TIMEOUT' and float(runtime) < (float(cutoff)-10):
                        print '[WARNING] Result for algorithm '+algorithm+' on instance '+instance+' is '+result+', but runtime ('+runtime+') is less than cutoff ('+cutoff+').'

                    #Make timed-out instance have around cutoff as runtime.
                    #if result == 'TIMEOUT':
                        #runtime = cutoff


                    rundata[instance][algorithm] = (result,runtime,quality,cutoff,seed,additional_rundata)

    return rundata


def writeZillaFormatRunResult(runresults,output_filename):
    """
    Writes a run result dictionary to a Zilla formatted run result file.

    runresults - a run results dictionary taking problem instance to a dictionary taking algorithm to
    a tuple (result,runtime,quality,cutoff,seed,additional run data).
        runresults[problem instance][algorithm] -> (result,runtime,quality,cutoff,seed,additional run data)
    output_filename - the name of the file to write.
    """

    #Build instance list.
    instances = sorted(runresults.keys())

    #Build algorithm list.
    algorithms = set()
    for instance in instances:
        algorithms.update(runresults[instance].keys())
    algorithms = sorted(list(algorithms))

    with open(output_filename,'wb') as csvfile:
        csvwriter = csv.writer(csvfile)
        #Write header
        csvwriter.writerow(['instance']+algorithms)
        #Write every result.
        for instance in instances:
            line = [instance]
            for algorithm in algorithms:
                if algorithm in runresults[instance].keys():
                    (result,runtime,quality,cutoff,seed,additional_rundata) = runresults[instance][algorithm]
                    result = ':'.join([result,runtime,quality,cutoff,seed])
                else:
                    result = DataConverter.ZF_INCOMPLETE_ALGORITHM_EXECUTION
                line.append(result)
            csvwriter.writerow(line)
    return


def writeZillaFormatFeatures(runresults,output_filename):
    """
    Writes a run result dictionary to a Zilla formatted feature file.

    runresults - a run results dictionary taking problem instance to a dictionary taking algorithm to
    a tuple (runtime,runresult,cutoff).
        runresults[problem instance][algorithm] -> (result,runtime,quality,cutoff,seed,additional run data)
    Moreover, the additional run data should be formatted as "feature1=value;feature2=value;..."
    output_filename - the name of the file to write.
    """

    #Build instance list.
    instances = sorted(runresults.keys())

    #Build algorithm list.
    algorithms = set()
    for instance in instances:
        algorithms.update(runresults[instance].keys())
    algorithms = sorted(list(algorithms))
    if not len(algorithms) == 1:
        raise Exception('There are more than one algorithms represented in the feature data.')
    else:
        algorithm = algorithms[0]

    features = set()
    featuredata = dict()
    #Convert run result to feature data
    for instance in instances:
        featuredatum = dict()
        (result,runtime,quality,cutoff,seed,additional_rundata) = runresults[instance][algorithm]
        if result == 'SAT':
            for featurevaluepair in additional_rundata.split(';'):
                featurename = featurevaluepair.split('=')[0]
                #The Steve fix.
                #print 'Instance: %s, Features: %s' % (str(instance),str(featurevaluepair))
                if featurename[0] == ',':
                    featurename = featurename[1:]

                featurevalue = featurevaluepair.split('=')[1]

                if featurename in featuredatum.keys():
                    raise Exception('Duplicate values for feature '+featurename+'.')
                else:
                    featuredatum[featurename]=featurevalue

                features.add(featurename)

            featuredata[instance]=(runtime,featuredatum)

        elif result == 'TIMEOUT' or float(runtime)>float(cutoff):
            featuredata[instance]=(runtime,dict())
        elif result == 'CRASHED':
            featuredata[instance]=(runtime,dict())
        else:
            raise Exception('Unrecognized result from algorithm '+algorithm+' on instance '+instance+' run results dictionary entry: '+str(runresults[instance][algorithm]))

    features = sorted(list(features))

    #Check if there are constant features.
    constantfeatures = []
    for feature in features:
        values = set()
        for instance in instances:
            (runtime,featuredatum) = featuredata[instance]
            if feature in featuredatum:
                values.add(featuredatum[feature])
        if len(values)<=1:
            print '[WARNING] Feature %s with values %s seems to be constant.' % (feature,str(values))
            constantfeatures.append(feature)

    if len(constantfeatures)>0:
        answer = DataConverter.queryYesNo('Should constant features be removed?')
        if answer:
            print 'Removing constant features.'
            for feature in constantfeatures:
                for instance in instances:
                    (runtime,featuredatum) = featuredata[instance]
                    if feature in featuredatum:
                        del featuredatum[feature]
            features = filter(lambda f : f not in constantfeatures,features)
        else:
            print 'Leaving constant features alone.'

    with open(output_filename,'wb') as csvfile:
        csvwriter = csv.writer(csvfile)
        #Write header
        csvwriter.writerow(['instance','runtime']+features)
        #Write every result.
        for instance in instances:
            (runtime,featuredatum) = featuredata[instance]
            line = [instance,runtime]
            for feature in features:
                if feature in featuredatum.keys():
                    line.append(featuredatum[feature])
                else:
                    line.append(DataConverter.ZF_INCOMPLETE_FEATURE_VALUE)

            csvwriter.writerow(line)
    return
