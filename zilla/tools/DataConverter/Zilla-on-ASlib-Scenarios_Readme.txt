The three steps to running zilla on an ASlib scenario.

We need an ASlib scenario folder called <ASlib scenario>, and assumes you have a zilla release in a folder called <zilla release> and that you want to put the zilla version of the ASlib scenario in a folder called <zilla scenario>.

1. Convert the ASlib scenario to a zilla scenario.

    > python <zilla release>/tools/DataConverter/ASlib2Zilla.py <ASlib scenario> <zilla scenario> 

This translates all the ASlib data and scenario information to formats zilla understands, and puts everything in <zilla scenario>

2. Run zilla on the created scenario, using test instances from fold <f> and all other instances as training.

    > cd <zilla scenario>
    > bash <zilla release>/zilla-builder --scenario <zilla scenario>/scenario_f<f>.txt 

This will build/train a zilla selector (might take some time) that will be placed in <zilla scenario>/zilla-output/<zilla scenario name>_<f> (where <zilla scenario name> is the name of the ASlib scenario), and also test the constructed zilla on the fold <f> test instances - the results of the testing will be in <zilla scenario>/zilla-output/<zilla scenario name>_<f>/TestResults.csv

[optional 3.] Convert the test results to a more maleable, non-zilla format, where every value type has its own column (instead of being separated by ":"'s).

	> python <zilla release>/tools/DataConverter/DataConverter.py ZFrt2manual <zilla scenario>/zilla-output/<zilla scenario name>_<f>/TestResults.csv <zilla scenario>/zilla-output/<zilla scenario name>_<f>/TestResults_manual.csv
	
You can find the converter file at <zilla scenario>/zilla-output/<zilla scenario name>_<f>/TestResults_manual.csv.