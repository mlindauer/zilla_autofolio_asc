#!/bin/bash

mkdir out_dir
python ../train.py -s CSP-2010-train/ -o ./out_dir
mkdir predictions
python ../test.py -s CSP-2010-test/ -m ./out_dir -o ./predictions/
