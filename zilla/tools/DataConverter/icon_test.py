'''
ICON Challenge on Algorithm Selection: Zilla Submission
Interface for calling zilla adhering to the ICON Challenge on Algorithm selection

Author : Chris Cameron
'''

import sys
import os
import argparse
import logging
import shutil
import random
import math
import random
import time
from subprocess import call
    

#Zilla data converter.
from ZillaConverter import *

import Zilla
from Zilla import *

#COSEAL ASlib data reader.
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)),'COSEAL-reader'))
from coseal_reader import *

def main():
    
    start_time = time.time()
    
    zilla_release = sys.argv[1]
    ASlib_scenario = sys.argv[2]
    zilla_folder = sys.argv[3] #+ str(random.randint(1,100000))
    feature_groups= sys.argv[4].split(',') #['Pre'] # Leave as None if unrestricted
    
    presolver = sys.argv[5]
    presolver_runtime = sys.argv[6]
    num_folds = 10
    
    zilla = Zilla(zilla_release, ASlib_scenario, zilla_folder, feature_groups, presolver, presolver_runtime, num_folds)
    
    (PAR10) = zilla.get_performance_estimate()
    #model = zilla.train()
    #file = zilla.test(zilla.zilla_folder)
    #print 'Output file printed to: %s' % file
    end_time = time.time()
    
    elapsed_time  = end_time - start_time

    output_filename = os.path.join('./', ASlib_scenario + '_' + '_'.join(feature_groups) + '_results.txt')
    print 'Writing to file: %s' % output_filename
    with open(output_filename,'wb') as output_file:
        #lines = []
        output_file.write('ASlib scenario: %s\n' % ASlib_scenario)
        output_file.write('Feature groups: %s\n' % str(feature_groups))
        output_file.write('Elapsed time: %f\n' % elapsed_time)
        lines= 'PAR10 score: %f\n' % PAR10
        #lines.append('Percent solved: %f' % percentSolved)        
        output_file.write(lines)
    
    print 'Shutting down...'
    return

if __name__ == '__main__':
    main()
    
