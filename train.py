#!/usr/local/bin/python2.7
# encoding: utf-8
'''
ZillaAutofolio -- Zilla and Autofolio entering the AS Challenge 2015

@author:     Marius Lindauer, Chris Cameron

@copyright:  2015 Marius Lindauer. All rights reserved.

@license:    GPLv3

@contact:    lindauer@cs.uni-freiburg.de    
'''

import sys
import os
import logging
import traceback
import random
import shutil
import arff # liac-arff
import time

from argparse import ArgumentParser, REMAINDER
from argparse import ArgumentDefaultsHelpFormatter

from autofolio.autofolio import ext_func

from zilla.tools.DataConverter.Zilla import *
#from Zilla import *

from subprocess import Popen, PIPE

__all__ = []
__version__ = 0.1
__date__ = '2015-07-02'
__updated__ = '2015-07-02'

class ZillaAutofolio(object):
    
    def __init__(self, root_path, args):
        ''' Constructor '''
        self.root_path = root_path
        self.scenario = args.scenario
        self.output_dir = args.output
        
        self.FLEX_PORTFOLIO = [#"",
                        "--aspeed-opt", # with static schedule; important for PREMARSHALLING
                        "--approach 'kNN' --kNN 1", # 1-NN 
                        "--approach 'kNN' --kNN 1 --aspeed-opt", # 1-NN with static schedule
                        "--approach 'kNN' --kNN 23", # 23-NN
                        "--approach 'kNN' --kNN 23 --aspeed-opt", # 23-NN  with static schedule
                          ]
        
        '''
        Zilla scenario construction
        '''
        print self.root_path
        zilla_release   = os.path.abspath(os.path.join(self.root_path, 'zilla'))
        zilla_output_folder = os.path.abspath(os.path.join(self.output_dir, 'zilla-scenario'))
        feature_groups = None # ['Pre']
        self.presolver = args.presolver
        self.presolver_runtime = args.time_presolver
        num_folds = 10 
        self.zilla = Zilla(zilla_release, self.scenario, zilla_output_folder, feature_groups, self.presolver, self.presolver_runtime, num_folds)
         
        self.BUDGET = 43200 # 12 h
        
        self.TRAIN_SLACK = 3600 # 1 h
        #self._AUTOFOLIO_SLACK = 44000 # only to test the script 
        
        self.__start_time = time.time()
        
    def main(self):
        ''' main method '''
    
        logging.info("Remaing time: %d (sec)" %(self.BUDGET - (time.time() - self.__start_time)))
        
        logging.info("Train FlexFolio with default configuration as a backup -- will not be used if everything else works as planned")
        self.train_flexfolio("")
        
        logging.info("Remaing time: %d (sec)" %(self.BUDGET - (time.time() - self.__start_time)))
        # evaluate default flexfolio
        logging.info("Evaluate Default Flexfolio Performance")
        def_perf = self.evaluate_flexfolio("") #default
        logging.info("Default flexfolio peformance: %.2f" %(def_perf))
        logging.info("Remaining time: %d (sec)" %(self.BUDGET - (time.time() - self.__start_time)))
        
        logging.info("Evaluate SATZilla performance using CV")
        zilla_perf = self.zilla.get_performance_estimate()
        logging.info("Performance of Zilla: %.2f" %(zilla_perf))
        
        logging.info("Remaining time: %d (sec)" %(self.BUDGET - (time.time() - self.__start_time)))
        logging.info("Evaluate different flexfolio configurations")
        perfs = [def_perf]
        for conf in self.FLEX_PORTFOLIO:
            remaining_time = self.BUDGET - (time.time() - self.__start_time)
            if remaining_time > self.TRAIN_SLACK:
                perf = self.evaluate_flexfolio(conf)
                perfs.append(perf)
                logging.info("flexfolio peformance: %.2f" %(perf))
            
        min_perf = min(perfs)
        min_perf_index = perfs.index(min_perf)
        if min_perf_index == 0:
            best_conf = ""
        else:
            best_conf = self.FLEX_PORTFOLIO[min_perf_index - 1]
            
        if zilla_perf < min_perf:
            logging.info("Best Configuration so far SATzilla")
        else:
            logging.info("Best Configuration so far: %s" %(best_conf))
            
        logging.info("Remaing time: %d (sec)" %(self.BUDGET - (time.time() - self.__start_time)))
        
        remaining_time = self.BUDGET - (time.time() - self.__start_time)
        af_perf = sys.maxint
        if remaining_time > self.TRAIN_SLACK: # start AutoFolio only if we have at least self.TRAIN_SLACK seconds left
            logging.info("Running Autofolio")
            try:
                af_conf = ext_func(scenario_dir=self.scenario, budget=int(remaining_time-self.TRAIN_SLACK), outdir=self.output_dir)
                af_perf = self.evaluate_flexfolio(af_conf)
                logging.info("Performance of AutoFolio: %.2f" %(af_perf))
            except TypeError:
                traceback.print_exc()
                logging.error("AutoFolio failed")
                af_perf = sys.maxint
        
        # Testing for zilla winning        
        #=======================================================================
        # af_perf = 100000000
        # min_perf = 10000000
        #=======================================================================
            
        logging.info("Remove all temporary files from output directory")
        try:
            for f in os.listdir(self.output_dir):
                if f != "zilla-scenario":
                    p = os.path.join(self.output_dir, f)
                    if os.path.isdir(p):
                        shutil.rmtree(p, ignore_errors=True)
                    else:
                        os.remove(p)
        except:
            traceback.print_exc()
        
        logging.info("#####################")    
        logging.info("## Final Performances")
        logging.info("#####################")
        try:
            logging.info("Flexfolio: %.2f" %(min_perf))
            logging.info("Zilla: %.2f" %(zilla_perf))
            logging.info("Autofolio: %.2f" %(af_perf))
        except: 
            traceback.print_exc() # just in case
        
        if af_perf < zilla_perf and af_perf < min_perf:
            logging.info("Final best configuration: %s" %(af_conf))
            self.train_flexfolio(af_conf)
        elif min_perf < zilla_perf:
            logging.info("Final best configuration: %s" %(best_conf))
            self.train_flexfolio(best_conf)
        else:
            logging.info("Final best configuration: SATzilla" )
            logging.info("Training zilla model...")
            model = self.zilla.train()
        
    def evaluate_flexfolio(self, conf):
        ''' 
            evaluate the performance of flexfolio with a given configuration <conf> 
        '''
        N_FOLDS = 10
        
        base_cmd = "python %s --aslib-dir %s --model-dir %s --crossfold %d --time-limit 100 %s " %(os.path.join(self.root_path, "selectors", "flexfolio", "src", "flexfolio_train.py"),
                                                                  self.scenario,
                                                                  self.output_dir,
                                                                  N_FOLDS,
                                                                  conf
                                                                  )
        if self.presolver:
            base_cmd += "--pre-solver %s,%s " %(self.presolver, self.presolver_runtime)
        
        logging.info("Calling: %s" %(base_cmd))
        
        p = Popen(base_cmd, shell=True, stdout=PIPE, stderr=PIPE)
        (stdout, stderr) = p.communicate()
        for line in stdout.split("\n"):
            if line.startswith("% PAR10 (without unsolved)"):
                return float(line.split(":")[1])
            
        logging.error("FlexFolio run probably crashed")
        return sys.maxint
        
    def train_flexfolio(self, conf):
        '''
            train flexfolio with <conf> for the AS Challenge 
        '''
        base_cmd = "python %s --aslib-dir %s --model-dir %s --time-limit 100 --train %s " %(os.path.join(self.root_path, "selectors", "flexfolio", "src", "flexfolio_train.py"),
                                                                  self.scenario,
                                                                  self.output_dir,
                                                                  conf
                                                                  )
        
        if self.presolver:
            base_cmd += "--pre-solver %s,%s " %(self.presolver, self.presolver_runtime)
        
        logging.info("Calling: %s" %(base_cmd))
        
        p = Popen(base_cmd, shell=True, stdout=PIPE, stderr=PIPE)
        (stdout, stderr) = p.communicate()

def main(argv=None): # IGNORE:C0111
    '''Command line options.'''

    if argv is None:
        argv = sys.argv
    else:
        sys.argv.extend(argv)

    #program_name = os.path.basename(sys.argv[0])
    program_version = "v%s" % __version__
    program_build_date = str(__updated__)
    program_version_message = '%%(prog)s %s (%s)' % (program_version, program_build_date)
    program_shortdesc = __import__('__main__').__doc__.split("\n")[1]
    program_license = '''%s

  Created by Marius Lindauer on %s.
  Copyright 2015. All rights reserved.

  Licensed under the GPLv3
  http://www.gnu.org/copyleft/gpl.html

  Distributed on an "AS IS" basis without warranties
  or conditions of any kind, either express or implied.

USAGE
''' % (program_shortdesc, str(__date__))

    try:
        
        root_path = os.path.join(os.path.split(sys.argv[0])[0])
        # Setup argument parser
        parser = ArgumentParser(description=program_license, formatter_class=ArgumentDefaultsHelpFormatter)
        
        parser.add_argument("-s", '--scenario', required=True, help="aslib scenario")
        parser.add_argument("-o", '--output', required=True, help="output for model files")
        parser.add_argument("-p", '--presolver', required=False, default=None, help="pre-solver")
        parser.add_argument("-t", '--time_presolver', required=False, default=None, help="time for pre-solver")
        parser.add_argument("-v", '--verbose', choices=["INFO", "DEBUG"], default="INFO", help="verbose level")
        
        # Process arguments
        args = parser.parse_args()
        logging.basicConfig(format="[%(levelname)s]: %(message)s", level=args.verbose)
        
        za = ZillaAutofolio(root_path, args)
        za.main()
        
    except KeyboardInterrupt:
        ### handle keyboard interrupt ###
        return 0
    except Exception, e:
        traceback.print_exc()
        return 1

if __name__ == "__main__":
    sys.exit(main())