'''
Create scripts for testing zilla for ICON Challenge
'''
 
import sys
import os
import argparse
import logging
import shutil
import random
import math

scenarios = ['ASP-POTASSCO',
             'CSP-2010',
             'MAXSAT12-PMS',
             'PREMARSHALLING-ASTAR-2013',
             'PROTEUS-2014',
             'QBF-2011',
             'SAT11-HAND',
             'SAT11-INDU',
             'SAT11-RAND',
             'SAT12-ALL',
             'SAT12-HAND',
             'SAT12-INDU',
             'SAT12-RAND']







feat_groups = {'ASP-POTASSCO':[['Dynamic-4'],
                      ['Dynamic-4','Dynamic-1'],
                      ['Dynamic-4','Dynamic-1','Dynamic-3'],
                      ['Dynamic-4','Dynamic-1','Dynamic-3','Dynamic-2'],
                      ['Static', 'Dynamic-1', 'Dynamic-2', 'Dynamic-3', 'Dynamic-4'],
                      ['Static', 'Dynamic-1', 'Dynamic-2', 'Dynamic-3']],
             'CSP-2010':[['all_feats']],
             'MAXSAT12-PMS':[['group_basics']],
             'PREMARSHALLING-ASTAR-2013':[['all']],
             'PROTEUS-2014':[['csp'],
                             ['csp','direct'],
                             ['csp','direct','support'],
                             ['csp','direct','support','directorder']],
             'QBF-2011':[['all_feats']],
             'SAT11-HAND':[['Pre','Basic'],
                           ['Pre','Basic', 'KLB'],
                           ['Pre', 'Basic', 'KLB'],
                           ['Pre', 'Basic', 'KLB', 'DIAMETER'],
                           ['Pre', 'Basic', 'KLB', 'DIAMETER', 'sp'],
                           ['Pre', 'Basic', 'KLB', 'DIAMETER', 'cl', 'sp'],
                           ['Pre', 'Basic', 'KLB', 'DIAMETER', 'cl', 'sp', 'ls_saps'],
                           ['Pre', 'Basic', 'KLB', 'DIAMETER', 'cl', 'sp', 'ls_saps', 'lobjois'],
                           ['Pre', 'Basic', 'KLB', 'DIAMETER', 'cl', 'sp', 'ls_saps', 'ls_gsat', 'lobjois'],
                           ['Pre', 'Basic', 'KLB', 'CG', 'DIAMETER', 'cl', 'sp', 'ls_saps', 'ls_gsat', 'lobjois'],
                           ['Pre', 'Basic', 'KLB', 'CG']],
             'SAT11-INDU':[['Pre','Basic'],
                           ['Pre','Basic', 'KLB'],
                           ['Pre', 'Basic', 'KLB'],
                           ['Pre', 'Basic', 'KLB', 'DIAMETER'],
                           ['Pre', 'Basic', 'KLB', 'DIAMETER', 'sp'],
                           ['Pre', 'Basic', 'KLB', 'DIAMETER', 'cl', 'sp'],
                           ['Pre', 'Basic', 'KLB', 'DIAMETER', 'cl', 'sp', 'ls_saps'],
                           ['Pre', 'Basic', 'KLB', 'DIAMETER', 'cl', 'sp', 'ls_saps', 'lobjois'],
                           ['Pre', 'Basic', 'KLB', 'DIAMETER', 'cl', 'sp', 'ls_saps', 'ls_gsat', 'lobjois'],
                           ['Pre', 'Basic', 'KLB', 'CG', 'DIAMETER', 'cl', 'sp', 'ls_saps', 'ls_gsat', 'lobjois'],
                           ['Pre', 'Basic', 'sp', 'ls_saps', 'cl', 'DIAMETER']],
             'SAT11-RAND':[['Pre','Basic'],
                           ['Pre','Basic', 'KLB'],
                           ['Pre', 'Basic', 'KLB'],
                           ['Pre', 'Basic', 'KLB', 'DIAMETER'],
                           ['Pre', 'Basic', 'KLB', 'DIAMETER', 'sp'],
                           ['Pre', 'Basic', 'KLB', 'DIAMETER', 'cl', 'sp'],
                           ['Pre', 'Basic', 'KLB', 'DIAMETER', 'cl', 'sp', 'ls_saps'],
                           ['Pre', 'Basic', 'KLB', 'DIAMETER', 'cl', 'sp', 'ls_saps', 'lobjois'],
                           ['Pre', 'Basic', 'KLB', 'DIAMETER', 'cl', 'sp', 'ls_saps', 'ls_gsat', 'lobjois'],
                           ['Pre', 'Basic', 'KLB', 'CG', 'DIAMETER', 'cl', 'sp', 'ls_saps', 'ls_gsat', 'lobjois'],
                           ['Pre', 'ls_gsat', 'sp', 'ls_saps']],
             'SAT12-ALL':[['Pre','Basic'],
                           ['Pre','Basic', 'KLB'],
                           ['Pre', 'Basic', 'KLB'],
                           ['Pre', 'Basic', 'KLB', 'DIAMETER'],
                           ['Pre', 'Basic', 'KLB', 'DIAMETER', 'sp'],
                           ['Pre', 'Basic', 'KLB', 'DIAMETER', 'cl', 'sp'],
                           ['Pre', 'Basic', 'KLB', 'DIAMETER', 'cl', 'sp', 'ls_saps'],
                           ['Pre', 'Basic', 'KLB', 'DIAMETER', 'cl', 'sp', 'ls_saps', 'lobjois'],
                           ['Pre', 'Basic', 'KLB', 'DIAMETER', 'cl', 'sp', 'ls_saps', 'ls_gsat', 'lobjois'],
                           ['Pre', 'Basic', 'KLB', 'CG', 'DIAMETER', 'cl', 'sp', 'ls_saps', 'ls_gsat', 'lobjois'],
                           ['Pre', 'KLB', 'Basic', 'lobjois', 'sp', 'ls_saps']],
             'SAT12-HAND':[['Pre','Basic'],
                           ['Pre','Basic', 'KLB'],
                           ['Pre', 'Basic', 'KLB'],
                           ['Pre', 'Basic', 'KLB', 'DIAMETER'],
                           ['Pre', 'Basic', 'KLB', 'DIAMETER', 'sp'],
                           ['Pre', 'Basic', 'KLB', 'DIAMETER', 'cl', 'sp'],
                           ['Pre', 'Basic', 'KLB', 'DIAMETER', 'cl', 'sp', 'ls_saps'],
                           ['Pre', 'Basic', 'KLB', 'DIAMETER', 'cl', 'sp', 'ls_saps', 'lobjois'],
                           ['Pre', 'Basic', 'KLB', 'DIAMETER', 'cl', 'sp', 'ls_saps', 'ls_gsat', 'lobjois'],
                           ['Pre', 'Basic', 'KLB', 'CG', 'DIAMETER', 'cl', 'sp', 'ls_saps', 'ls_gsat', 'lobjois'],
                           ['Pre', 'Basic', 'CG', 'DIAMETER', 'lobjois', 'ls_saps', 'sp']],
             'SAT12-INDU':[['Pre','Basic'],
                           ['Pre','Basic', 'KLB'],
                           ['Pre', 'Basic', 'KLB'],
                           ['Pre', 'Basic', 'KLB', 'DIAMETER'],
                           ['Pre', 'Basic', 'KLB', 'DIAMETER', 'sp'],
                           ['Pre', 'Basic', 'KLB', 'DIAMETER', 'cl', 'sp'],
                           ['Pre', 'Basic', 'KLB', 'DIAMETER', 'cl', 'sp', 'ls_saps'],
                           ['Pre', 'Basic', 'KLB', 'DIAMETER', 'cl', 'sp', 'ls_saps', 'lobjois'],
                           ['Pre', 'Basic', 'KLB', 'DIAMETER', 'cl', 'sp', 'ls_saps', 'ls_gsat', 'lobjois'],
                           ['Pre', 'Basic', 'KLB', 'CG', 'DIAMETER', 'cl', 'sp', 'ls_saps', 'ls_gsat', 'lobjois'],
                           ['Pre', 'ls_gsat', 'Basic', 'lobjois', 'sp', 'cl', 'CG']],
             'SAT12-RAND':[['Pre','Basic'],
                           ['Pre','Basic', 'KLB'],
                           ['Pre', 'Basic', 'KLB'],
                           ['Pre', 'Basic', 'KLB', 'DIAMETER'],
                           ['Pre', 'Basic', 'KLB', 'DIAMETER', 'sp'],
                           ['Pre', 'Basic', 'KLB', 'DIAMETER', 'cl', 'sp'],
                           ['Pre', 'Basic', 'KLB', 'DIAMETER', 'cl', 'sp', 'ls_saps'],
                           ['Pre', 'Basic', 'KLB', 'DIAMETER', 'cl', 'sp', 'ls_saps', 'lobjois'],
                           ['Pre', 'Basic', 'KLB', 'DIAMETER', 'cl', 'sp', 'ls_saps', 'ls_gsat', 'lobjois'],
                           ['Pre', 'Basic', 'KLB', 'CG', 'DIAMETER', 'cl', 'sp', 'ls_saps', 'ls_gsat', 'lobjois'],
                           ['Pre', 'KLB', 'Basic']]}

presolvers = {'ASP-POTASSCO':'clasp/2.1.3/h8-n1',
             'CSP-2010':'',
             'MAXSAT12-PMS':'',
             'PREMARSHALLING-ASTAR-2013':'astar-symmullt-transmul',
             'PROTEUS-2014':'choco',
             'QBF-2011':'QuBE',
             'SAT11-HAND':'sattime+_2011-03-02',
             'SAT11-INDU':'',
             'SAT11-RAND':'adaptg2wsat2011_2011-03-02',
             'SAT12-ALL':'tnm',
             'SAT12-HAND':'sparrow',
             'SAT12-INDU':'lrglshr',
             'SAT12-RAND':''}

presolver_runtimes = {'ASP-POTASSCO':'4.0',
                      'CSP-2010':'',
                      'MAXSAT12-PMS':'',
                      'PREMARSHALLING-ASTAR-2013':'307.0',
                      'PROTEUS-2014':'2060.0',
                      'QBF-2011':'299.0',
                      'SAT11-HAND':'254.0',
                      'SAT11-INDU':'',
                      'SAT11-RAND':'33.0',
                      'SAT12-ALL':'5.0',
                      'SAT12-HAND':'16.0',
                      'SAT12-INDU':'5.0',
                      'SAT12-RAND':''}

scenario_dir = '/ubc/cs/home/c/cchris13/arrow-space/zilla/icon_testing/aslib_data-aslib-v1.0.1'
zilla_release = '/ubc/cs/home/c/cchris13/arrow-space/zilla/icon_testing/zilla'
call_strings_dir = '/ubc/cs/home/c/cchris13/arrow-space/zilla/icon_testing'
icon_scenario_filename = os.path.join(call_strings_dir, 'template.sh')

# python icon_test.py %RELEASE %SCENARIO %ZILLA_OUTPUT %FEAT_GROUPS %PRESOLVER %PRESOLVER_RUNTIME

for scenario in scenarios:
    for feat_group in feat_groups[scenario]:
        with open(icon_scenario_filename,'rb') as icon_scenario_file:
            content = icon_scenario_file.read()
            content = content.replace('%SCENARIO',os.path.join(scenario_dir,scenario))
            content = content.replace('%FEAT_GROUPS', ','.join(feat_group))
            content = content.replace('%RELEASE',zilla_release)
            content = content.replace('%ZILLA_OUTPUT',os.path.join(call_strings_dir,scenario + '_' + '_'.join(feat_group).replace('.','_')))
            content = content.replace('%PRESOLVER',presolvers[scenario])
            content = content.replace('%PRESOLVE_RUNTIME',presolver_runtimes[scenario])


        zilla_scenario_filename = os.path.join(call_strings_dir,scenario + '_' + '_'.join(feat_group).replace('.','_') + '.sh')
        with open(zilla_scenario_filename,'wb') as zilla_scenario_file:
            zilla_scenario_file.write(content)

        print 'Writing file for scenario: ' + zilla_scenario_filename
