#!/usr/local/bin/python2.7
# encoding: utf-8
'''
ICON Challenge on Algorithm Selection: Zilla Submission
Interface for calling zilla adhering to the ICON Challenge on Algorithm selection

@author : Chris Cameron

@contact: cchris13@cs.ubc.ca
'''

import sys
import os
import argparse
import logging
import shutil
import random
import math
import subprocess
import numpy
from subprocess import call
from subprocess import check_output
    

#Zilla data converter.
from ZillaConverter import *

sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))

from ZillaRunDataAnalyzer import *

import ASlibZillaConverter
from ASlibZillaConverter import *

#COSEAL ASlib data reader.
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)),'COSEAL-reader'))
from coseal_reader import *

logging.basicConfig(level=logging.INFO)


class Zilla(object):
    '''
        information about a zilla selection scenario
    '''
    def __init__(self, zilla_release, scenario,zilla_folder, feature_groups, presolver, presolver_runtime, num_folds):
        
        self.zilla_release = zilla_release
        self.scenario = scenario
        self.feature_groups = feature_groups
        self.zilla_folder = zilla_folder
        self.presolver = presolver
        self.presolver_runtime = presolver_runtime
        self.num_folds = num_folds
        
        if self.presolver:
            self.add_empty_presolver = 'false'
        else:
            self.add_empty_presolver = 'true'
            # Ensure runtime is also None
            if self.presolver_runtime:
                raise Exception('Presolver is None but presolver runtime is: %s' % self.presolver_runtime)
            self.presolver = ""
            self.presolver_runtime = "0.0"
        
        logging.info('Converting ASlib scenario into zilla scenario...')
        ICON_ASlib_to_Zilla(self,num_folds)
        
        
    '''
    Return a PAR10 performance estimate for zilla
    '''
    def get_performance_estimate(self):
        
        performances = []
        for fold in range (1,self.num_folds+1):
            
            logging.info("Running CV fold %d out of %d..." % (fold, self.num_folds))
            
            # Get the CV zilla scenario file
            scenario_name = "%s/scenario_cv_f%d.txt" % (self.zilla_folder, fold)
            if not os.path.isfile(scenario_name):
                raise Exception('Expected scenario file at: %s, but file not found!' % scenario_name)
            
            # Get name and seed from file
            (rungroup,seed) = self.get_scenario_info(scenario_name)
            
            # Train zilla scenario        
            call_string = ["cd", "%s" % (self.zilla_folder), ";","%s/bin/zilla-builder" % (self.zilla_release), "--scenario","%s" % (scenario_name) , "--add-empty-presolving-schedule", "%s" % (self.add_empty_presolver)]
            call_string = " ".join(call_string)
            logging.info("Executing Call String: " + str(call_string))
            with open(os.devnull, "w") as f:
                output = call(call_string,stderr=subprocess.STDOUT,stdout=f,shell=True)
            
            # Extract performance results
            results_file = os.path.join(self.zilla_folder, 'zilla-output/%s/TestResults.csv' % (rungroup))
            if not os.path.isfile(results_file):
                raise Exception('No TestResults.csv file at %s. Cannot evaluate performance!' % (results_file))
            run_data = os.path.join(self.zilla_folder, 'algo-runs.csv')
            (instancesSolved, PAR10) = self.getResults(results_file, run_data)
            performances.append(PAR10)
            logging.info("PAR10 estimate: %f in fold %d" % (PAR10, fold))
            
        logging.info('CV performances: %s' % str(performances))
        
        meanPAR10 = sum(performances)/len(performances)
        stdPAR10 = numpy.std(performances)
        
        logging.info('PAR10 performance\n Mean: %f\nstd.dev: %f' % (meanPAR10, stdPAR10))
            
        return sum(performances)/len(performances)
    
    def getResults(self,zilla_result, run_result):
        
        rawdata = readZillaRunData([run_result])
        unsolved_instances = self.getUnsolvedInstances(rawdata)
        rundata = readZillaRunData([zilla_result])
        
        # Remove unsolved instances (instances that can't be solved by any algorithm)
        instances = list(set(rundata.keys()) - set(unsolved_instances))
        
        logging.debug('Making sure all instances have been executed by all algorithms...')
        algorithms = []
        for instance in instances:
            if not(len(algorithms) == 0 or rundata[instance].keys() == algorithms):
                raise Exception('Instance '+instance+' has not been executed by the same algorithms ('+str(rundata[instance].keys())+') as the other instances ('+str(algorithms)+').')
            algorithms = rundata[instance].keys()
        
        meanPAR10 = getMeanPARk(rundata,algorithms,instances,10)[0]
        numberofsolved = getNumberOfSolved(rundata,algorithms,instances)[0]
        
        
        return (float(numberofsolved)/len(instances), meanPAR10)
     
    def getUnsolvedInstances(self,runresults):
        """
        Return a list of instances that are not solved by any algorithm.
    
        runresults - a run results dictionary taking problem instance to a dictionary taking algorithm to
        a tuple (runtime,runresult,cutoff).
            runresults[problem instance][algorithm] -> (runtime,runresult,cutoff)
        """
        unsolved_instances = []
        instances = runresults.keys()
        for instance in instances:
            unsolved = True
            for algorithm in runresults[instance].keys():
                (result,runtime,quality,cutoff,seed) = runresults[instance][algorithm]
                if result == 'SAT' or result == 'UNSAT':
                    unsolved = False
                    break
            if unsolved:
                unsolved_instances.append(instance)
    
        return unsolved_instances   
    
    '''
    Train a zilla model on scenario
    Returns path to zilla model (path to directory containing serialized zilla models)
    '''    
    def train(self):
        
        # Check if scenario has training data. Won't in ICON testing phase.
        if not os.path.isfile(os.path.join(self.zilla_folder,'algo-runs.csv')):
            raise Exception('No algo-runs.csv file. Missing run data to train model!')
        scenario_name = "%s/scenario_train.txt" % (self.zilla_folder)
        if not os.path.isfile(scenario_name):
            raise Exception('Expected training scenario file at: %s, but file not found!' % scenario_name)
        
         # Get name and seed from file
        (rungroup,seed) = self.get_scenario_info(scenario_name)

        # Train zilla scenario
        call_string = ["cd", "%s" % (self.zilla_folder),";","%s/bin/zilla-builder" % (self.zilla_release), "--scenario" ,"%s" % (scenario_name),"--add-empty-presolving-schedule", "%s" % (self.add_empty_presolver)]
        call_string = " ".join(call_string)
        logging.info('Training Zilla model...')
        with open(os.devnull, "w") as f:
            output = call(call_string,stderr=subprocess.STDOUT,stdout=f,shell=True)
      
        # Copy model to zilla output folder
        model_folder = self.zilla_folder + '/zilla-output/%s/zilla-egg%s' % (rungroup,seed)
        dest = os.path.join(self.zilla_folder, 'egg')
        if os.path.exists(dest):
            shutil.rmtree(dest)
        logging.info('...Model building complete. Model folder saved to %s' % dest)    
        shutil.copytree(model_folder, dest)
        
        return model_folder
    
    '''
    Tests provided zilla model on scenario
    '''      
    def test(self, train_dir):
        
        scenario_name = "%s/scenario_test.txt" % (self.zilla_folder)
        if not os.path.isfile(scenario_name):
            raise Exception('Expected testing scenario file at: %s, but file not found!' % scenario_name)
        
        (rungroup,seed) = self.get_scenario_info(scenario_name)
        
        # Get directory containing zilla model
        model_dir = os.path.abspath(os.path.join(train_dir, 'egg'))
        if not os.path.isdir(model_dir):
            raise Exception('Expected zilla egg model at directory: %s, but no directory found!' % model_dir)
        
        # Execute zilla testing phase with provided model
        logging.info('Testing Zilla from model directory: %s', model_dir)
        call_string = ["cd", "%s" % (self.zilla_folder),";","%s/bin/zilla-icon-selector" % (self.zilla_release), "--scenario", "%s" % (scenario_name), "--egg", "%s" % (model_dir)]
        call_string = " ".join(call_string)
        with open(os.devnull, "w") as f:
            output = call(call_string,stderr=subprocess.STDOUT,stdout=f,shell=True)
      
        #print output
        
        # Ensuring prediction file exists
        experiment_dir = os.path.join(os.path.join(self.zilla_folder ,'zilla-output'), rungroup)
        
        predictions_file = os.path.join(experiment_dir, 'predictions.csv')
        logging.info('Saving output prediction file to: %s', predictions_file)
        if not os.path.isfile(predictions_file):
            raise Exception('Expected predictions output file at: %s, but no file found!' % predictions_file)
        
        
        
        # Copy predictions.csv file to parent directory of zilla scenario folder
        parent_dir = os.path.dirname(self.zilla_folder)
        shutil.copy(predictions_file, parent_dir)
        
        return predictions_file
    
    def get_scenario_info(self,scenario_name):
                 # Get name and seed from file
        rungroup=""
        seed=""        
        file = open(scenario_name, 'rU')
        for line in file.readlines():
            #print line
            line = line.replace('\n','')
            if "rungroup-name=" in line:
                rungroup = line.split("=",1)[1]
                rungroup = rungroup[1:-1]
            elif "seed=" in line:
                seed = line.split("=",1)[1]
        file.close() 
        
        return (rungroup,seed)
    

        

        
 
        
    
        
    
    