from DataConverter import *

def getReadConversions():
    return []
def getWriteConversions():
    return 'manual'

def writeManualRunResult(runresults,output_filename):
    """
    Writes a run result dictionary to a processable csv file (one column per run values).

    runresults - a run results dictionary taking problem instance to a dictionary taking algorithm to
    a tuple (runtime,runresult,cutoff).
        runresults[problem instance][algorithm] -> (result,runtime,quality,cutoff,seed,additional run data)
    output_filename - the name of the file to write.
    """

    #Build instance list
    instances = sorted(runresults.keys())

    #Build algorithm list.
    algorithms = set()
    for instance in instances:
        algorithms.update(runresults[instance].keys())
    algorithms = sorted(list(algorithms))

    with open(output_filename,'wb') as csvfile:
        csvwriter = csv.writer(csvfile)
        #Write header
        header = ['instance']
        for algorithm in algorithms:
            header.append('%s:runtime' % algorithm)
            header.append('%s:result' % algorithm)
            header.append('%s:quality' % algorithm)
            header.append('%s:cutoff' % algorithm)
            header.append('%s:seed' % algorithm)
        csvwriter.writerow(header)
        for instance in instances:
            line = [instance]
            for algorithm in algorithms:
                if algorithm in runresults[instance].keys():
                    (result,runtime,quality,cutoff,seed,additional_rundata) = runresults[instance][algorithm]
                else:
                    (result,runtime,quality,cutoff,seed,additional_rundata) = ['UNKNOWN']*6;
                line.extend([runtime,result,quality,cutoff,seed])
            csvwriter.writerow(line)

    return

def readManualRunResults(input_filenames):
    """
    Reads a (list of) processable type data files into a run results dictionary taking problem instance to a dictionary taking algorithm to
    a tuple (runtime,runresult,cutoff).

        runresults[problem instance][algorithm] -> (result,runtime,quality,cutoff,seed,additional run data)


    input_filenames - a list of names of files in manual format.
    """

    runresults = dict()

    for input_filename in input_filenames:

        print 'Reading data from',input_filename,'...'

        with open(input_filename,'r') as csvfile:
            csvreader = csv.reader(csvfile)

            #Process header
            header = csvreader.next()
            header = header[1:]

            algoindices = dict()
            for i in range(0,len(header),5):
                algorithm = ':'.join(header[i].split(':')[:-1])

                algoindices[i]=algorithm

                if ':runtime' not in header[i]:
                    raise Exception('Could not find runtime column for algorithm %s.' % algorithm)
                if ':result' not in header[i+1]:
                    raise Exception('Could not find result column for algorithm %s.' % algorithm)
                if ':quality' not in header[i+2]:
                    raise Exception('Could not find quality column for algorithm %s.' % algorithm)
                if ':cutoff' not in header[i+3]:
                    raise Exception('Could not find cutoff column for algorithm %s.' % algorithm)
                if ':seed' not in header[i+4]:
                    raise Exception('Could not find seed column for algorithm %s.' % algorithm)

            for row in csvreader:
                instance = row[0]

                if instance not in runresults.keys():
                    runresults[instance] = dict()

                for algoindex in algoindices.keys():
                    algorithm = algoindices[algoindex]

                    runtime = row[1+algoindex]
                    result = row[1+algoindex+1]
                    quality = row[1+algoindex+2]
                    cutoff = row[1+algoindex+3]
                    seed = row[1+algoindex+4]

                    additional_rundata = ''

                    if algorithm in runresults[instance].keys():
                        raise Exception('Run data for algorithm '+algorithm+' on instance '+instance+' appears twice.')
                    if result == 'TIMEOUT' and float(runtime) < (float(cutoff)-10):
                        print '[WARNING] Result for algorithm '+algorithm+' on instance '+instance+' is '+result+', but runtime ('+runtime+') is less than cutoff ('+cutoff+').'

                    runresults[instance][algorithm] = (result,runtime,quality,cutoff,seed,additional_rundata)

    return runresults
