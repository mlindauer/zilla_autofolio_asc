import sys
import os
import csv
import operator
import copy

import numpy as np
#import matplotlib
#matplotlib.use('Agg')
#import matplotlib.pyplot as plt
#from matplotlib import cm
##import mypyutils
import scipy.stats as stats

def main():
    input_filenames = sys.argv[1:]
    
    print input_filenames

    print 'Reading in data...'
    rundata = readZillaRunData(input_filenames)
    instances = rundata.keys()

    print 'Making sure all instances have been executed by all algorithms...'
    algorithms = []
    for instance in instances:
        if not(len(algorithms) == 0 or rundata[instance].keys() == algorithms):
            raise Exception('Instance '+instance+' has not been executed by the same algorithms ('+str(rundata[instance].keys())+') as the other instances ('+str(algorithms)+').')
        algorithms = rundata[instance].keys()

    winning_instances_per_algorithm = {}
    approximately_winning_instances_per_algorithm = {}
    num_solved_per_instance = {}

    for algorithm in algorithms:
        winning_instances_per_algorithm[algorithm] = 0
        approximately_winning_instances_per_algorithm[algorithm] = 0

    for instance in instances:
        bestAlg = ''
        bestResult = ''
        bestRuntime = 10000000000000000.0
        bestQuality = 10000000000000000.0
        computedCutoff = 1000000000000.0
        computedSeed = 100000000
        numSolved = 0

        for algorithm in algorithms:
            (result,runtime,quality,cutoff,seed) = rundata[instance][algorithm]
            computedCutoff = cutoff
            computedSeed = seed
            
            if (runtime < bestRuntime and (result == 'SAT' or result == 'UNSAT')):
                bestAlg = algorithm
                bestRuntime = runtime
                bestQuality = quality
                bestResult = result
                numSolved += 1

        num_solved_per_instance[instance] = numSolved

        # result:runtime:quality:cutoff:seed
        if (bestAlg != ''):
            print "%s,%s:%.4f:%.4f:%.4f:%d" % (instance,bestResult,bestRuntime,bestQuality,computedCutoff,computedSeed)
        else:
            print "%s,TIMEOUT:%.4f:%.4f:%.4f:%d" % (instance,computedCutoff,-1.0,computedCutoff,computedSeed)

        for algorithm in algorithms:
            (result,runtime,quality,cutoff,seed) = rundata[instance][algorithm]

            if (len(bestAlg) > 0 and (result == 'SAT' or result == 'UNSAT') and runtime <= bestRuntime+0.001):
                winning_instances_per_algorithm[algorithm] += 1
            if (len(bestAlg) > 0 and (result == 'SAT' or result == 'UNSAT') and runtime <= (bestRuntime+0.001)*1.05):
                approximately_winning_instances_per_algorithm[algorithm] += 1

    print '%-20s %30s' % ('n', 'Number of instances solved by at least n planners')
    for i in range(1,len(algorithms)+1):
        count = 0
        for instance in instances:
            numSolved = num_solved_per_instance[instance]
            if (numSolved >= i):
                count += 1

        print '%-20s %30s' % (str(i), str(count))


    print '%-20s %30s %50s' % ('Algorithm', 'Number of instances won', 'Number of instances within 5% of winner')
    print '-'*120
    for algorithm in algorithms:
        print '%-50s %30d %50d' % (algorithm, winning_instances_per_algorithm[algorithm], approximately_winning_instances_per_algorithm[algorithm])


    if len(algorithms)>1:
        print 'Computing marginal solver contributions'

        marginal_vbs_names = []
        marginal_vbs_rundata = {}
        for instance in instances:
            marginal_vbs_rundata[instance] = {}

        for algorithm in algorithms:
            marginal_data = copy.deepcopy(rundata)
            for instance in instances:
                marginal_data[instance].pop(algorithm)

            vbs_name = 'VBS ex %s' % (algorithm)
            addVBS(marginal_data, vbs_name)

            for instance in instances:
                marginal_vbs_rundata[instance][vbs_name] = marginal_data[instance][vbs_name]

            marginal_vbs_names.append(vbs_name)

        print 'Adding VBS to data...'
        addVBS(rundata, 'VBS')
        algorithms.append('VBS')

        print 'Adding marginal VBSs'
        for instance in instances:
            for marginal in marginal_vbs_names:
                rundata[instance][marginal] = marginal_vbs_rundata[instance][marginal]

        algorithms.extend(marginal_vbs_names)

    print 'Analyzing data...'

    print 'generating PAR10 scores'
    meanPAR10s = getMeanPARk(rundata,algorithms,instances,10)
    performance = {algorithms[i] : meanPAR10s[i] for i in range(len(algorithms))}

    print 'generating number of solved instances'
    numberofsolved = getNumberOfSolved(rundata,algorithms,instances)
    numberofsolved = {algorithms[i]:numberofsolved[i] for i in range(len(algorithms))}

    print 'generating median runtimes'
    medianruntimes = getMedianRuntimes(rundata,algorithms,instances)
    medianruntimes = {algorithms[i]:medianruntimes[i] for i in range(len(algorithms))}

    print '%-50s %20s %20s %20s %20s' % ('Algorithm', 'Runtime PAR10', 'Solved Instances (num)', 'Solved Instances (%)', 'Median Runtime')
    print '-'*120
    for algorithm in sorted(performance,key=performance.get):
        print '%-50s %20.2f %20d %20.1f%% %20.1f s' % (algorithm,performance[algorithm],numberofsolved[algorithm],float(numberofsolved[algorithm])/float(len(instances))*100,medianruntimes[algorithm])


    #Comparing algorithms

    runtimesall = []
    for algorithm in algorithms:
        runresults = getRunresults(rundata,algorithm,instances)
        runtimes = []
        for (result,runtime,quality,cutoff,seed) in runresults.values():
            if runtime > cutoff or not(result == 'SAT' or result == 'UNSAT'):
                runtimes.append(cutoff)
            else:
                runtimes.append(runtime)

        runtimesall.append(runtimes)


    #Plotting runtime ECDFs
    #plt.figure()
    #for i in range(len(algorithms)):
    #    mypyutils.pyplot.plotECDF(runtimesall[i],cm.jet(1.*i/len(algorithms)),label=algorithms[i])
    #plt.xlabel('Runtime (s)')
    #plt.xscale('log')
    #plt.title('Runtime ECDFs')
    #plt.legend(loc='upper left',numpoints=1)
    #plt.grid()
    #plt.show()

    return

def readZillaRunData(input_filenames):
    """
    Read in a list of Zilla run data files as a run data dictionary (taking instances to algorithm to run result data).

    input_filenames - a list of Zilla run data filenames.
    """
    rundata = dict()

    for input_filename in input_filenames:
        print 'Reading data from',input_filename,'...'
        with open(input_filename,'rb') as csvfile:
            csvreader = csv.reader(csvfile,delimiter=',',quotechar='"')

            #Read the header for algorithm names.
            header = csvreader.next()

            algorithms = header[1:]

            for line in csvreader:
                try:
                    instance = line[0]

                    if not len(header) == len(line):
                        raise Exception('A line in Zilla run data file '+input_filename+' has not the same length as the header.')

                    if instance not in rundata.keys():
                        rundata[instance] = dict()

                    runresults = line[1:]
                    for i in range(len(algorithms)):
                        algorithm = algorithms[i]
                        runresult = runresults[i]

                        if runresult == 'NA':
                            continue
                        try:
                            (result,runtime,quality,cutoff,seed) = runresult.split(':')
                        except Exception as e:
                            print 'Could not parse run result:'
                            print runresult
                            raise e

                        if algorithm in rundata[instance].keys():
                            raise Exception('Run data for algorithm '+algorithm+' on instance '+instance+' appears twice.')

                        #Changed crashed runs runtime to cutoff.
                        if result == 'CRASHED':
                            runtime = cutoff

                        #Check validity of runtime when TIMEOUT
                        if result == 'TIMEOUT' and float(runtime) < (float(cutoff)-10):
                            print '[WARNING] Result for algorithm '+algorithm+' on instance '+instance+' is '+result+', but runtime ('+runtime+') is less than cutoff ('+cutoff+').'

                        #Make timed-out instance have around cutoff as runtime.
                        if result == 'TIMEOUT':
                            runtime = cutoff


                        rundata[instance][algorithm] = (result,float(runtime),float(quality),float(cutoff),long(seed))

                except Exception as e:
                    print 'Could not parse line:'
                    print line
                    raise e

    return rundata


def addVBS(rundata, vbs, components = None):
    """
    Add Virtual Best Solver (VBS) data in the given run data. The VBS minimizes runtime and quality across
    all the available algorithms for a given instance.

    rundata - the rundata dictionary.
    """

    for instance in rundata.keys():
        vbsruntime = None
        vbsquality = None
        vbsresult = None
        algorithms = components if components != None else rundata[instance].keys()
        for algorithm in algorithms:
            (result,runtime,quality,cutoff,seed) = rundata[instance][algorithm]
            if vbsresult == None or vbsresult == 'CRASHED' or result == 'SAT' or result == 'UNSAT':
                if vbsruntime == None or vbsruntime > runtime:
                    vbsresult = result
                    vbsruntime = runtime
            if vbsquality == None or vbsquality > quality:
                vbsquality = quality

        if vbs in rundata[instance].keys():
            raise Exception('Algorithm VBS already in run data for instance '+instance+'.')

        rundata[instance][vbs] = (vbsresult,vbsruntime,vbsquality,cutoff,seed)



def getRunresults(rundata,algorithm,instances):
    """
    Return a dictionary from instance to run result for the given algorithm, across the given instances.

    rundata - the rundata dictionnary.
    algorithms - an algorithm to get run results for.
    instances - list of instances to evaluate.
    """
    runresults = {}

    for instance in instances:
        runresults[instance] = rundata[instance][algorithm]

    return runresults

def getNumberOfSolved(rundata,algorithms,instances):
    """
    Return an array of number of solved instances, where the i-th element of the array is the number
    of instances solved by the i-th algorithm in the given algorithms across all given instances.

    rundata - the rundata dictionnary.
    algorithms - list of algorithms to find mean PAR10 scores for.
    instances - list of instances to evaluate.
    """
    numberofsolved = []

    for algorithm in algorithms:
        solved = 0
        for instance in instances:
            (result,runtime,quality,cutoff,seed) = rundata[instance][algorithm]
            if not(runtime > cutoff or not(result == 'SAT' or result =='UNSAT')):
                solved+=1
        numberofsolved.append(solved)

    return numberofsolved

def getMedianRuntimes(rundata,algorithms,instances):
    """
    Return an array of median runtimes, where the i-th element of the array is the median
    runtime of the i-th algorithm in the given algorithms across all the given instances.

    rundata - the rundata dictionnary.
    algorithms - list of algorithms to find mean PAR10 scores for.
    instances - list of instances to evaluate.
    """
    medianruntimes = []

    for algorithm in algorithms:
        runtimes = []
        for instance in instances:
            (result,runtime,quality,cutoff,seed) = rundata[instance][algorithm]
            if runtime > cutoff or not(result == 'SAT' or result == 'UNSAT'):
                runtimes.append(cutoff)
            else:
                runtimes.append(runtime)

        medianruntimes.append(np.median(runtimes))

    return medianruntimes

def getMeanPARk(rundata,algorithms,instances,k):
    """
    Return an array of PAR-k scores for runtime, where the i-th element of the array
    is the PAR-k score of the i-th algorithm in the given algorithms across all given instances.

    rundata - the rundata dictionnary.
    algorithms - list of algorithms to find mean PAR10 scores for.
    instances - list of instances to evaluate.
    k - the PAR penalty.
    """
    meanPARk = []
    print algorithms
    for algorithm in algorithms:
        PARks = []
        for instance in instances:
            (result,runtime,quality,cutoff,seed) = rundata[instance][algorithm]
            if runtime > cutoff or not(result == 'SAT' or result == 'UNSAT'):
                PARk = cutoff*k
            else:
                PARk = runtime
            PARks.append(PARk)
        meanPARk.append(np.mean(PARks))

    return meanPARk


if __name__ == '__main__':
    main()
